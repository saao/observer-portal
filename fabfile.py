from portal import __version__
from fabric.api import cd, lcd, run, prefix, local
from fabric.contrib.console import confirm


def lint():
    """Inspect the code for style violations."""

    local('flake8 --ignore=E126,E128,E501 --exclude=portal/migrations,docs --max-line-length=100 --exit-zero .')


def test(report=False):
    """Execute unit tests and collect coverage data."""

    local('SECRET_KEY=secret coverage run --source="." manage.py test')
    if report:
        local('coverage report')


def build():
    with lcd('docs'):
        local('make html')


def deploy(remote='origin', branch='master'):
    """Deploys a new version to the remote host.

    Specify a host or a list of hosts on the command line like this:

        $ fab deploy --hosts=user@host1,user@host2

    """

    changes = local('git log --oneline {}/{}..HEAD'.format(remote, branch), capture=True)
    if len(changes) > 0:
        print(changes)
        print('')
        if confirm('Would you like to push these unpushed changes?', default=True):
            local('git push {} {}'.format(remote, branch))

    with cd('/var/www/portal.saao.ac.za/src'):
        run('git fetch origin master')
        run('git checkout origin/master')
        with prefix('source /var/www/portal.saao.ac.za/venv/bin/activate'):
            run('pip install -r requirements.txt')
            run('./manage.py migrate')
            run('./manage.py collectstatic -v0 --noinput')
    run('touch /var/www/portal.saao.ac.za/src/portal/wsgi.py')
    run('touch /var/www/portal.saao.ac.za/src/it/wsgi.py')
    run('touch /var/www/portal.saao.ac.za/src/sites/admin/wsgi.py')


def release(version=None):
    """Creates a new release.

    It may be useful to chain this with the deploy command:

        $ fab --hosts=user@host release:version=1.2.3 deploy

    """

    if not version:
        version = raw_input('Please enter a version ({}): '.format(__version__))

    with open('./portal/__init__.py', 'w') as f:
        f.write("__version__ = '{}'".format(version))

    local('git commit portal/__init__.py -m "Release {}"'.format(version))
    local('git tag -a -m "Release v{0}" v{0}'.format(version))
