from django.contrib.auth import views
from django.conf import settings
from django.conf.urls import include, url
from portal.views import profile
from portal.auth.forms import LoginForm


urlpatterns = [
    url(r'^', include(
        'portal.maintenance.urls', namespace='maintenance')),
    url(r'^profile/$', profile, name='profile'),

    url(r'^admin/', include('portal.maintenance.admin.urls', namespace='admin')),

    url(r'^login/$', views.login,
        {'template_name': 'auth/login.html', 'authentication_form': LoginForm}, name='login'),
    url(r'^logout/$', views.logout, {'next_page': '/'}, name='logout'),
]


if settings.DEBUG:
    from django.views import static
    from django.views.generic.base import TemplateView
    urlpatterns += [
        url(r'^403$', TemplateView.as_view(template_name='403.html')),
        url(r'^404$', TemplateView.as_view(template_name='404.html')),
        url(r'^500$', TemplateView.as_view(template_name='500.html')),
        url(r'^docs/(?P<path>.*)$', static.serve, {
            'document_root': 'docs/_build/html/',
        }),
        url(r'^media/(?P<path>.*)$', static.serve, {
            'document_root': settings.MEDIA_ROOT,
        }),
        url(r'^static/(?P<path>.*)$', static.serve, {
            'document_root': settings.STATIC_ROOT,
        }),
   ]
