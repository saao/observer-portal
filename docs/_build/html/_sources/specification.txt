Specification
=============

The observer portal is a general purpose web application and a central point of
entry meant to be used at the SAAO to manage its observing activities.


Features
--------

* Telescope Time Applications
* ROTA
* Transport and Accommodation
* Observer Log
* Night Log
* Observer Feedback Form
* Fault Reporting
* Reports
* Daily Maintenance Log
* Telescope and instrument checklists?
* Central standby ROTA for all staff/departments (Electronics, IT, Mechanical)

* Data flow from Sutherland, telephone pins and accounting as well as a link
  back to documentation (on wikis), the intranet, etc.


Telescope Time Applications
***************************

A call for proposals goes out to mailing lists consisting of SAAO staff,
previous observers and various other people. When an application is submitted,
a notification is sent to observer@saao.ac.za and the principal contact listed
on the application receives a confirmation email. Each proposal is assigned a
unique reference number.

The approval process starts with a technical feasibility evaluation which is
currently done by Hannah and Lisa. Based on certain technical criteria the
reviewers indicate whether an application is technically feasible or not with
an optional comment.

The Time Assessment Committee (TAC) then evaluate and score applications based
on other criteria such as scientific merit. Each member of the committee is
given the opportunity to score every application for the trimester with a
required comment. A member of the TAC cannot score applications they are
involved in. Scores are not restricted within a predefined range, so TAC
members can each use their own rating system. Because of this scores need to be
normalised afterwards.  When this is done the head of the committee compiles a
consolidated report for each? proposal with a recommendation to approve or
reject an application.

Applications then need to be allocated a time slot by Ramotholo. Allocations
are currently done in one week blocks but it should be possible to allocate
blocks of varying sizes. Depending on the nature of the proposal multiple
blocks can be allocated with different observers for each block.

When an application is allocated time on a telescope, notify the principal
investigator (PI) via email. They then need to accept the time slot(s) that
they were given and confirm the observer(s) for each block. Send the PI a
confirmation email explaining the next steps. Also notify staff that were
selected to assist the observer on the first night of their run.


The telescopes and instruments that the system should cater for are:

* 1.0m
    * SHOC
    * STE3
    * STE4
* 1.9m
    * GIRAFFE
    * SHOC
    * SpCCD
    * STE3
    * STE4
    * HIPPO
* IRSF
    * SIRIUS
* LCOGT
    * Sinistro
    * SBG
* KMTNET


Roles:

* Observer
* Proposal coordinator
    * Ramotholo
* Proposal reviewer - Technical
    * Hannah
    * Lisa
* Proposal reviewer - TAC
    * Amanda
    * ...
* TAC Head


NOTES:

* The 0.5m 0.75m telescopes have been decommissioned
* Calls will go out in trimesters from July 2015 (Trimester 1: Jan - Apr, Trimester 2: May - Aug, and Trimester 3: Sep - Dec)
* The existing electronic form can be seen `here <http://www.saao.ac.za/science/observing/online-telescope-time-application/>`_
* For an example of the email sent out to potential observers, see the `CFP <http://ctfileserver.cape.saao.ac.za/staff/briehan/SAAO/portal/cfp>`_
  file.
* For an example of the technical feasibility evaluation criteria, see
  `2013_Q4_Proposals.xlsx <http://ctfileserver.cape.saao.ac.za/staff/briehan/SAAO/portal/2013_Q4_Proposals.xlsx>`_
* Time application deadlines are as follows:
    * 15 September for Jan/Apr observing, and
    * 15 January for May/Aug observing.
    * 15 May for Sep/Dec observing;
* IRSF applications are reviewed by colleagues at Kyoto University and do not
  follow the SAAO approval process.
* Send out an announcement when instruments or telescopes are made unavailable.
* Service observing? http://tops.saao.ac.za/
    * Ramotholo has indicated that it would be useful for these applications to
      also be done through the portal, but it's not a priority right now.
      Apparently entries go into a database and approved ones show up in the
      appropriate dome where the observer can then evaluate them and observe
      the listed targets. Speak to Christian about how it works.
* Duplicate previous proposals to quickly compose a new one
* Use automatic email to observers reminding them to complete feedback form (or
  anything else?) with the ability to turn off these reminders.


QUESTIONS:

* Automatic/manual CFP with strict deadline?
* More detail required on the TAC approval process.
* More detail required on the steps following the observer's acceptance of
  allocated time.
* Technicians need access too (check with Hannah to make sure why).
* Nazli needs access to student-related data once an application is approved
  (check with her for what purpose).
* Should the hosted telescopes be in the system too?
    * LCOGT (Yes)
    * ...
* Should students and other applicants be given access to the observer portal?
    * Should accounts be created for them automatically if they don't already
      have one? And should they be notified when this happens?


ROTA
****

The observing calendar or "ROTA" is populated automatically based on time
allocations during the telescope time application process above. Observers
should be able to view their own observing calendar while logged in, but the
calendar should also be publicly visible.

It displays a month view of a typical calendar showing which observers have
been allocated time on a telescope and which instrument they require. The
following is also displayed:

* The percentage of Moon illuminated at local midnight.
* Names of support astronomers and actual observers.
* Public holidays.


QUESTIONS:

* The ROTA currently also lists SALT allocations but the observer portal only
  handles time applications for the small telescopes. Do we still need this and
  will it need to be populated manually?


Transport and Accommodation
***************************

Samantha contacts Ross Pinto or Vic from Koornhoop Manor House to arrange for
airport transfers. If accommodation is required in Cape Town she allocates
rooms for the observers or arranges for accommodation off-site at Koornhoop.

Samantha needs to receive a confirmation from Magdalena or Vic.

Observers need to enter any dietary restrictions they might have for their stay
in Sutherland.

Foreign observers need to supply a copy of their passports with an expiry date.
When they apply again and their passports have expired they need to provide an
updated copy.

An email needs to go out to Magdalena, the hostel supervisor, so that she can
assign rooms.

Circulate a preliminary transport schedule for the next week on Tuesdays and a
final list the following Tuesday morning. Include transport@saao.ac.za,
transport@list.saao.ac.za, the driver and all other people not included. Send
out updated list if any changes are made.


Rooms:

* Cape Town
    * Off-site
        * Koornhoop
    * Jacaranda (1-5)
    * Riverside (1-4)
    * Stables (1-3)

* Sutherland
    * Astronomer wing (1-8)
    * Day rooms (1-5)
    * KMTNET (1-2)
    * SAAO chalet (1-2)
    * SALT chalet (1-2)
    * Student wing (1-4)
    * Technical wing (1-6)


Vehicles:

* Quantum (Cape Town/Sutherland Shuttle)
* Quantum astrobus
* Avanza 1
* Avanza 2
* Site bakkie
* Livina
* Verso


Roles:

* Hostel Supervisor
    * Magdalena
* Travel and Liaison Officer
    * Samantha


Observer Log
************

TODO:

NOTES:

* See `Observing_Rec.xls <http://ctfileserver.cape.saao.ac.za/staff/briehan/SAAO/portal/Observing_Rec.xls>`_ for an example.


Night Log
*********

TODO:


Observer Feedback Form
**********************

When an observer completes their run, they are required to fill out a feedback
form detailing their experience at the telescope and in Sutherland.

NOTES:

* The existing electronic form can be seen `here <http://www.saao.ac.za/science/observing/feedback/>`_


Fault Reporting
***************

TODO:


Reports
*******

Generate reports based on the observer logs for Ramotholo by trimester. See
`40Inch.xlsx` and `74Inch.xlsx` for examples.

TODO:

* Find out what other (if any) reports are required.


Daily Maintenance Log
*********************

We'd like to create a daily log for work done on the SAAO telescopes and hosted
facilities. This is currently done for SALT (so you can look at those to get
an idea of what we are thinking). We think the bulk of the effort for
creating and distributing a daily log could be taken on by having a website for
information entry, and the compiled information submitted each day should be
sent out via email. The IT, Mechanical, and Electronics standby people are
expected to fill out the log each day, and if they do not it should also be
recorded (so that people are aware that no info was provided). Ramotholo would
be the person monitoring the process and following up to ensure that it is
working.

Initial specs (open to discussion!):

(1) Hosted on the intranet.
(2) Inputs should be kept simple (to promote easy use!).

Need to have the following inputs:  (i) a drop-down menu of who is filing (IT
Standby, Electronics Standby, Mechanical Standby, Other), (ii) text boxes for
entry for the 74 in telescope, the 40 in telescope, the IRSF, and Other (for
the last, they should have to type in what facility is being mentioned). Notes
on completing the text box should say to provide a brief description of the
work, any known issues or problems, and who to contact (if other than standby).

(3) The information should be compiled into an email, which is broken into
sections for each of the facilities (74in, 40in, IRSF, and other). The email
should be sent at the end of the day (17:00?). For testing, the email can go
to Amanda, Ramotholo, and Steve Potter. Eventually, it will likely be all
Electronics, IT, TOPS, and some others. Notably, if there in no input from any
of the standbys, that should be highlighted at the top of the message (e.g. "No
input received from Electronics standby today.")

(4) Post the email on a website that is accessible to observers. Would a wiki
work for this? The emails/logs should be stored somewhere, but this website can
update daily.


Technical
---------

* `Django <http://djangoproject.com>`_
* `MySQL <http://mysql.com>`_
* `Admin LTE <http://almsaeedstudio.com/AdminLTE/index.html>`_ theme
* `Bitbucket <https://bitbucket.org/saao/observer-portal>`_
* `Sphinx <http://sphinx-doc.org>`_ for documentation

Some useful resources:

* `Official Django documentation <https://docs.djangoproject.com/en/>`_
* `The Django book <http://www.djangobook.com>`_
* `Official git documentation <http://git-scm.com/documentation>`_

Tasks
*****

The following section details the individual tasks needed to implement each
feature in as much detail as possible.

TODO:

* Technical and end-user documentation

Data model
^^^^^^^^^^

Identify and create each
`model <https://docs.djangoproject.com/en/1.6/#the-model-layer>`_ in the system.

* User
    * name
    * email
    * phone
    * address
    * copy of license (needed for designated drivers)
    * copy of passport (international observers only)
    * dietary requirements (needed for Magdalena at the hostel)
    * roles (M2M)
    * active?

* User Role
    * Administrator
    * Observer
    * TAC member
    * Hostel supervisor

* Telescope
    * name
    * available?
    * instruments (M2M)
    * URL (link to documentation)

* Instrument
    * name
    * available?
    * URL (link to documentation)

* Proposal (telescope applications)
    * reference (format: "{PI-lastname}-{year}-{month}-{telescope}")
    * title
    * abstract
    * public abstract
    * scientific and technical justification (file)
    * targets
        * name
        * right ascension
        * declination
        * vmag
        * comment
    * applicants
        * first name
        * last name
        * institution
        * observer?
        * email address?
    * principal investigator
        * first name
        * last name
        * phone
        * email
        * postal address
            * street
            * street2
            * city
            * postal code
            * country
    * previous applications for this project at the SAAO
        * reference
        * date awarded
        * useful percentage
        * comments
    * other applications this quarter at other observatories
        * telescope/satellite
        * title
    * students
        * first name
        * last name
        * email address?
    * supervisor
        * first name
        * last name
        * email address?
    * magnitude range
        * minimum
        * maximum
        * filter
    * requested time (in weeks)
        * dark
        * grey
        * bright
    * minimum useful time (in weeks)
        * dark
        * grey
        * bright
    * additional time (in weeks)
        * dark
        * grey
        * bright
    * preferred moon quarter grey time
        * first, third or "don't care"
    * preferred dates
    * impossible dates
    * justification for impossible dates
    * other time constraints
    * required support
    * state
        * draft, submitted, feasible, approved, allocated, accepted or declined
    * date created
    * date updated

* TODO: Event (for calendar)


Authentication and authorization
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

A user should be able to:

* Register a new account
    * register with an email address and a password
    * their account is inactive at first
    * a message is displayed telling them that an activation email has been
      sent to their email address
    * send out activation email with activation link
    * the user clicks the link which then activates their account and redirects
      them to the login form
    * they can log in using the credentials they provided at registration

* Log into their account using their email address and password
    * if the login fails for any reason just display a message saying that they
      didn't enter the correct details (or something along those lines)

* Log out of their account
    * a link on the portal directs them to a URL where their session is
      destroyed
    * they are then redirected back to the login page (or the home page of the
      portal if there is one)

* Reset their password
    * they can click through from the login form to the reset form
    * they then provide their email address
    * if they enter a valid email address that belongs to an active user in the
      database
        * we display a message saying that a reset link has been sent to their
          email address
    * send out reset email with reset link
    * they click on the reset link and we display a form where they can choose
      a new password
    * once they have typed and submitted their new password we redirect them to
      the login page where they can log in using their new password

Sessions should not be timed out automatically but should only last for the
lifetime of the browser instance currently running. The reason for this
approach is so that an observer at the telescope can keep the session running
if necessary without having to worry about the system logging them out.

TODO:

* User roles
    * Superuser -- access to everything
    * Observer
        * add and edit proposals (time applications)
        * accept or decline time allocated for proposal
        * add and edit observer log
        * add and edit night log
        * send feedback via observer feedback form
        * report faults
    * Proposal coordinator
        * Ramotholo
    * Proposal reviewer - Technical
        * Hannah
        * Lisa
    * Proposal reviewer - TAC
        * Amanda
        * ...
    * TAC Head
    * Transport and Accommodation
        * Samantha Fishley
    * Hostel supervisor
        * Magdalena
* Implement 2 x `authentication backends <https://docs.djangoproject.com/en/1.6/topics/auth/customizing/#authentication-backends>`_
    * Authenticate against internal/local database.
    * With an LDAP fallback for staff.

Telescope time applications
^^^^^^^^^^^^^^^^^^^^^^^^^^^

* Create, edit and submit applications as observer
* Approve/decline application as technical reviewer
* Approve/decline application as TAC member
* Allocate time for application as [Ramotholo's Role]
* Accept or decline allocated time as observer
* Notifications:
    * application submission confirmation to observer
    * to observer@saao.ac.za (which should be configurable)
    * to technical reviewers
    * to [Ramotholo] once applications are approved by technical reviewers
    * to TAC members once application has been assigned to them for reviewing
    * to [Ramotholo] once applications are approved by TAC
    * to [Magdalena's Role] once applications have been allocated time and have been accepted by the observer
    * to staff that need to assist observer

NOTES:

* See the `state transition diagram <http://ctfileserver.cape.saao.ac.za/staff/briehan/SAAO/portal/proposals.png>`_
  for an idea of the various states and transitions of the application process.

ROTA
^^^^

* Populate calendar when telescope time is allocated
* View calendar from within portal
* Public view of observing calendar

NOTES:

* See https://nylas.com/blog/rrules
