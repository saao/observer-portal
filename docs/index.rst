.. SAAO Observer Portal documentation master file, created by
   sphinx-quickstart on Mon Jul  6 15:28:48 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SAAO Observer Portal's documentation!
================================================

Contents:

.. toctree::
   :maxdepth: 2

   specification



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

