# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import os
import pytz

from polymorphic.models import PolymorphicModel
from django.db import models
from django.conf import settings
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.utils.encoding import python_2_unicode_compatible
from django_fsm import FSMField, transition


COUNTRIES = [
    ('AF', 'Afghanistan'),
    ('AX', 'Åland Islands'),
    ('AL', 'Albania'),
    ('DZ', 'Algeria'),
    ('AS', 'American Samoa'),
    ('AD', 'Andorra'),
    ('AO', 'Angola'),
    ('AI', 'Anguilla'),
    ('AQ', 'Antarctica'),
    ('AG', 'Antigua and Barbuda'),
    ('AR', 'Argentina'),
    ('AM', 'Armenia'),
    ('AW', 'Aruba'),
    ('AU', 'Australia'),
    ('AT', 'Austria'),
    ('AZ', 'Azerbaijan'),
    ('BS', 'Bahamas'),
    ('BH', 'Bahrain'),
    ('BD', 'Bangladesh'),
    ('BB', 'Barbados'),
    ('BY', 'Belarus'),
    ('BE', 'Belgium'),
    ('BZ', 'Belize'),
    ('BJ', 'Benin'),
    ('BM', 'Bermuda'),
    ('BT', 'Bhutan'),
    ('BO', 'Bolivia, Plurinational State of'),
    ('BQ', 'Bonaire, Sint Eustatius and Saba'),
    ('BA', 'Bosnia and Herzegovina'),
    ('BW', 'Botswana'),
    ('BV', 'Bouvet Island'),
    ('BR', 'Brazil'),
    ('IO', 'British Indian Ocean Territory'),
    ('BN', 'Brunei Darussalam'),
    ('BG', 'Bulgaria'),
    ('BF', 'Burkina Faso'),
    ('BI', 'Burundi'),
    ('KH', 'Cambodia'),
    ('CM', 'Cameroon'),
    ('CA', 'Canada'),
    ('CV', 'Cabo Verde'),
    ('KY', 'Cayman Islands'),
    ('CF', 'Central African Republic'),
    ('TD', 'Chad'),
    ('CL', 'Chile'),
    ('CN', 'China'),
    ('CX', 'Christmas Island'),
    ('CC', 'Cocos (Keeling) Islands'),
    ('CO', 'Colombia'),
    ('KM', 'Comoros'),
    ('CG', 'Congo'),
    ('CD', 'Congo, the Democratic Republic of the'),
    ('CK', 'Cook Islands'),
    ('CR', 'Costa Rica'),
    ('CI', 'Côte d\'Ivoire'),
    ('HR', 'Croatia'),
    ('CU', 'Cuba'),
    ('CW', 'Curaçao'),
    ('CY', 'Cyprus'),
    ('CZ', 'Czech Republic'),
    ('DK', 'Denmark'),
    ('DJ', 'Djibouti'),
    ('DM', 'Dominica'),
    ('DO', 'Dominican Republic'),
    ('EC', 'Ecuador'),
    ('EG', 'Egypt'),
    ('SV', 'El Salvador'),
    ('GQ', 'Equatorial Guinea'),
    ('ER', 'Eritrea'),
    ('EE', 'Estonia'),
    ('ET', 'Ethiopia'),
    ('FK', 'Falkland Islands (Malvinas)'),
    ('FO', 'Faroe Islands'),
    ('FJ', 'Fiji'),
    ('FI', 'Finland'),
    ('FR', 'France'),
    ('GF', 'French Guiana'),
    ('PF', 'French Polynesia'),
    ('TF', 'French Southern Territories'),
    ('GA', 'Gabon'),
    ('GM', 'Gambia'),
    ('GE', 'Georgia'),
    ('DE', 'Germany'),
    ('GH', 'Ghana'),
    ('GI', 'Gibraltar'),
    ('GR', 'Greece'),
    ('GL', 'Greenland'),
    ('GD', 'Grenada'),
    ('GP', 'Guadeloupe'),
    ('GU', 'Guam'),
    ('GT', 'Guatemala'),
    ('GG', 'Guernsey'),
    ('GN', 'Guinea'),
    ('GW', 'Guinea-Bissau'),
    ('GY', 'Guyana'),
    ('HT', 'Haiti'),
    ('HM', 'Heard Island and McDonald Islands'),
    ('VA', 'Holy See (Vatican City State)'),
    ('HN', 'Honduras'),
    ('HK', 'Hong Kong'),
    ('HU', 'Hungary'),
    ('IS', 'Iceland'),
    ('IN', 'India'),
    ('ID', 'Indonesia'),
    ('IR', 'Iran, Islamic Republic of'),
    ('IQ', 'Iraq'),
    ('IE', 'Ireland'),
    ('IM', 'Isle of Man'),
    ('IL', 'Israel'),
    ('IT', 'Italy'),
    ('JM', 'Jamaica'),
    ('JP', 'Japan'),
    ('JE', 'Jersey'),
    ('JO', 'Jordan'),
    ('KZ', 'Kazakhstan'),
    ('KE', 'Kenya'),
    ('KI', 'Kiribati'),
    ('KP', 'Korea, Democratic People\'s Republic of'),
    ('KR', 'Korea, Republic of'),
    ('KW', 'Kuwait'),
    ('KG', 'Kyrgyzstan'),
    ('LA', 'Lao People\'s Democratic Republic'),
    ('LV', 'Latvia'),
    ('LB', 'Lebanon'),
    ('LS', 'Lesotho'),
    ('LR', 'Liberia'),
    ('LY', 'Libya'),
    ('LI', 'Liechtenstein'),
    ('LT', 'Lithuania'),
    ('LU', 'Luxembourg'),
    ('MO', 'Macao'),
    ('MK', 'Macedonia, the former Yugoslav Republic of'),
    ('MG', 'Madagascar'),
    ('MW', 'Malawi'),
    ('MY', 'Malaysia'),
    ('MV', 'Maldives'),
    ('ML', 'Mali'),
    ('MT', 'Malta'),
    ('MH', 'Marshall Islands'),
    ('MQ', 'Martinique'),
    ('MR', 'Mauritania'),
    ('MU', 'Mauritius'),
    ('YT', 'Mayotte'),
    ('MX', 'Mexico'),
    ('FM', 'Micronesia, Federated States of'),
    ('MD', 'Moldova, Republic of'),
    ('MC', 'Monaco'),
    ('MN', 'Mongolia'),
    ('ME', 'Montenegro'),
    ('MS', 'Montserrat'),
    ('MA', 'Morocco'),
    ('MZ', 'Mozambique'),
    ('MM', 'Myanmar'),
    ('NA', 'Namibia'),
    ('NR', 'Nauru'),
    ('NP', 'Nepal'),
    ('NL', 'Netherlands'),
    ('NC', 'New Caledonia'),
    ('NZ', 'New Zealand'),
    ('NI', 'Nicaragua'),
    ('NE', 'Niger'),
    ('NG', 'Nigeria'),
    ('NU', 'Niue'),
    ('NF', 'Norfolk Island'),
    ('MP', 'Northern Mariana Islands'),
    ('NO', 'Norway'),
    ('OM', 'Oman'),
    ('PK', 'Pakistan'),
    ('PW', 'Palau'),
    ('PS', 'Palestine, State of'),
    ('PA', 'Panama'),
    ('PG', 'Papua New Guinea'),
    ('PY', 'Paraguay'),
    ('PE', 'Peru'),
    ('PH', 'Philippines'),
    ('PN', 'Pitcairn'),
    ('PL', 'Poland'),
    ('PT', 'Portugal'),
    ('PR', 'Puerto Rico'),
    ('QA', 'Qatar'),
    ('RE', 'Réunion'),
    ('RO', 'Romania'),
    ('RU', 'Russian Federation'),
    ('RW', 'Rwanda'),
    ('BL', 'Saint Barthélemy'),
    ('SH', 'Saint Helena, Ascension and Tristan da Cunha'),
    ('KN', 'Saint Kitts and Nevis'),
    ('LC', 'Saint Lucia'),
    ('MF', 'Saint Martin (French part)'),
    ('PM', 'Saint Pierre and Miquelon'),
    ('VC', 'Saint Vincent and the Grenadines'),
    ('WS', 'Samoa'),
    ('SM', 'San Marino'),
    ('ST', 'Sao Tome and Principe'),
    ('SA', 'Saudi Arabia'),
    ('SN', 'Senegal'),
    ('RS', 'Serbia'),
    ('SC', 'Seychelles'),
    ('SL', 'Sierra Leone'),
    ('SG', 'Singapore'),
    ('SX', 'Sint Maarten (Dutch part)'),
    ('SK', 'Slovakia'),
    ('SI', 'Slovenia'),
    ('SB', 'Solomon Islands'),
    ('SO', 'Somalia'),
    ('ZA', 'South Africa'),
    ('GS', 'South Georgia and the South Sandwich Islands'),
    ('SS', 'South Sudan'),
    ('ES', 'Spain'),
    ('LK', 'Sri Lanka'),
    ('SD', 'Sudan'),
    ('SR', 'Suriname'),
    ('SJ', 'Svalbard and Jan Mayen'),
    ('SZ', 'Swaziland'),
    ('SE', 'Sweden'),
    ('CH', 'Switzerland'),
    ('SY', 'Syrian Arab Republic'),
    ('TW', 'Taiwan, Province of China'),
    ('TJ', 'Tajikistan'),
    ('TZ', 'Tanzania, United Republic of'),
    ('TH', 'Thailand'),
    ('TL', 'Timor-Leste'),
    ('TG', 'Togo'),
    ('TK', 'Tokelau'),
    ('TO', 'Tonga'),
    ('TT', 'Trinidad and Tobago'),
    ('TN', 'Tunisia'),
    ('TR', 'Turkey'),
    ('TM', 'Turkmenistan'),
    ('TC', 'Turks and Caicos Islands'),
    ('TV', 'Tuvalu'),
    ('UG', 'Uganda'),
    ('UA', 'Ukraine'),
    ('AE', 'United Arab Emirates'),
    ('GB', 'United Kingdom'),
    ('US', 'United States'),
    ('UM', 'United States Minor Outlying Islands'),
    ('UY', 'Uruguay'),
    ('UZ', 'Uzbekistan'),
    ('VU', 'Vanuatu'),
    ('VE', 'Venezuela, Bolivarian Republic of'),
    ('VN', 'Viet Nam'),
    ('VG', 'Virgin Islands, British'),
    ('VI', 'Virgin Islands, U.S.'),
    ('WF', 'Wallis and Futuna'),
    ('EH', 'Western Sahara'),
    ('YE', 'Yemen'),
    ('ZM', 'Zambia'),
    ('ZW', 'Zimbabwe'),
]


class Address(models.Model):
    street = models.CharField('Street', max_length=255, null=True, blank=True)
    street2 = models.CharField('Street', max_length=255, null=True, blank=True)
    city = models.CharField('City', max_length=255, null=True, blank=True)
    postal_code = models.CharField('Postal Code', max_length=255, null=True, blank=True)
    country = models.CharField('Country', max_length=2, choices=COUNTRIES, null=True, blank=True)
    phone = models.CharField('Phone', max_length=255, null=True, blank=True)

    class Meta:
        abstract = True

    @property
    def postal_address(self):
        address_lines = [self.street]
        if self.street2:
            address_lines.append(self.street2)
        return ','.join(address_lines)


def get_avatar_name(instance, filename):
    filename = 'avatars/{}{}'.format(instance.user.id, os.path.splitext(filename)[1])
    filepath = os.path.join(settings.MEDIA_ROOT, filename)
    if os.path.exists(filepath):
        os.remove(filepath)
    return filename


@python_2_unicode_compatible
class Department(models.Model):
    class Meta:
        ordering = ['name']
        app_label = 'portal'

    name = models.CharField(max_length=255)
    parent = models.ForeignKey('self', null=True, blank=True, related_name='children')
    head = models.ForeignKey(settings.AUTH_USER_MODEL)

    def __str__(self):
        return self.name


class UserProfile(Address, models.Model):
    """User profile information.

    We define these properties in a separate model from the main User model to
    keep the auth app decoupled."""

    institution = models.CharField(max_length=255, blank=True, default='')

    dietary_requirements = models.CharField(
        max_length=255, blank=True, default='')
    timezone = models.CharField(max_length=64, default='UTC', choices=[
        (tz, tz) for tz in pytz.common_timezones])
    avatar = models.ImageField(
        upload_to=get_avatar_name, blank=True, null=True, default=None)

    department = models.ForeignKey('Department', blank=True, null=True)

    user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name='profile')


@python_2_unicode_compatible
class Instrument(models.Model):
    class Meta:
        ordering = ['name']

    name = models.CharField(max_length=255)
    is_available = models.BooleanField(default=False)
    status = models.CharField(max_length=255, blank=True)

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class Telescope(models.Model):
    class Meta:
        ordering = ['name']

    name = models.CharField(max_length=255)
    is_available = models.BooleanField(default=False)
    status = models.CharField(max_length=255, blank=True)
    instruments = models.ManyToManyField(Instrument, blank=True)

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class Applicant(models.Model):
    first_name = models.CharField('First Name', max_length=255)
    last_name = models.CharField('Last Name', max_length=255)
    email = models.EmailField(null=True)
    institution = models.CharField(max_length=255)
    application = models.ForeignKey('Application', related_name='applicants')
    is_observer = models.BooleanField(default=False)
    role = models.CharField(max_length=10, default='applicant',
        choices=[
            ('applicant', 'Applicant'),
            ('student', 'Student'),
            ('supervisor', 'Supervisor')])

    def __str__(self):
        return ' '.join([self.first_name, self.last_name])


@python_2_unicode_compatible
class Facility(models.Model):
    """Represents a group of telescopes.

    Determines which telescopes and instruments are available for applications
    at this facility.
    """

    class Meta:
        ordering = ['name']

    name = models.CharField(max_length=255)
    telescopes = models.ManyToManyField('Telescope', related_name='facilities')

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class Call(models.Model):
    """Represents a call for proposals."""

    class Meta:
        ordering = ['-deadline']

    name = models.CharField(max_length=255)
    deadline = models.DateTimeField()
    facility = models.ForeignKey('Facility', related_name='calls')

    created_by = models.ForeignKey(settings.AUTH_USER_MODEL)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class Application(PolymorphicModel, Address):
    """Represents applications for use of a telescope."""

    class Meta:
        permissions = (
            ('submit_application', 'Can submit applications'),
            ('approve_application', 'Can approve applications'),
            ('reject_application', 'Can reject applications'),

            # For coordinators, reviewers and time assessment committee.
            ('view_application', 'Can view a submitted application'),
        )
        ordering = ['-created_at']

    # When a user first creates an application we link it to a specific
    # facility.
    facility = models.ForeignKey('Facility', related_name='applications')

    # When an application is submitted we link it to whatever the latest call
    # is for the above facility.
    call = models.ForeignKey('Call', related_name='applications',
        null=True, default=None)

    reference = models.CharField('Reference',
        max_length=255, unique=True, null=True)

    telescope = models.ForeignKey(Telescope, related_name='applications',
        null=True, blank=True, default=None)
    instrument = models.ForeignKey(Instrument, related_name='applications',
        null=True, blank=True, default=None)

    # Principal contact.
    first_name = models.CharField(
        'First Name', max_length=255, blank=True, default='')
    last_name = models.CharField(
        'Last Name', max_length=255, blank=True, default='')
    email = models.EmailField(blank=True, default='')

    title = models.CharField('Title', max_length=255,
        null=False, blank=False, default='Untitled')
    abstract = models.TextField('Abstract', null=True, blank=True, default='')
    abstract_public = models.TextField('Summary for the General Public',
        null=True, blank=True, default='')
    scientific_justification = models.FileField(
        'Scientific and Technical Justification', null=True, blank=True)

    gratings_filters = models.CharField(
        'Gratings and Filters', max_length=255, blank=True, default='')

    # Magnitude range.
    magnitude_range_min = models.FloatField('Minimum (Brightest)',
        null=True, blank=True, default=0)
    magnitude_range_max = models.FloatField('Maximum (Faintest)',
        null=True, blank=True, default=0)
    magnitude_range_filter = models.CharField(
        'Filter', max_length=255, null=True, blank=True, default='')

    snr = models.PositiveIntegerField('SNR', null=True, blank=True, default=0)

    # Requested time (in weeks).
    time_dark = models.FloatField('Dark', blank=True, null=True)
    time_grey = models.FloatField('Grey', blank=True, null=True)
    time_bright = models.FloatField('Bright', blank=True, null=True)

    # Minimum useful time (in weeks).
    time_min_dark = models.FloatField('Dark', blank=True, null=True)
    time_min_grey = models.FloatField('Grey', blank=True, null=True)
    time_min_bright = models.FloatField('Bright', blank=True, null=True)

    # Additional time (in weeks).
    time_additional_dark = models.FloatField('Dark', blank=True, default=0)
    time_additional_grey = models.FloatField('Grey', blank=True, default=0)
    time_additional_bright = models.FloatField('Bright', blank=True, default=0)

    time_grey_moon_quarter = models.PositiveIntegerField(
        'Preferred Moon Quarter Grey Time', default=None, choices=[
            (1, 'First quarter'),
            (2, 'Third quarter'),
            (3, 'I don\'t care'),],
        null=True, blank=True)

    dates_preferred = models.CharField(
        'Preferred Dates', max_length=255, blank=True,
        default='')
    dates_impossible = models.CharField(
        'Impossible Dates', max_length=255, blank=True,
        default='')
    dates_impossible_justification = models.TextField(
        'Justification for Impossible Dates', blank=True, default='')

    time_constraints_other = models.TextField(
        'Other Time Constraints', blank=True, default='')

    # Use a nullable boolean because we don't know if applications required
    # support before we added this field.
    is_support_required = models.NullBooleanField()
    required_support = models.TextField(
        'Required Support', blank=True, default='')

    state = FSMField(default='draft')

    created_by = models.ForeignKey(settings.AUTH_USER_MODEL)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.reference or '-'

    @property
    def observers(self):
        return [applicant for applicant in self.applicants.all() if
            applicant.is_observer]

    @transition(field=state, source='submitted', target='approved', permission='portal.approve_application')
    def approve(self):
        pass

    @transition(field=state, source='submitted', target='rejected', permission='portal.reject_application')
    def reject(self):
        pass


@python_2_unicode_compatible
class Target(models.Model):
    """A list of targets for an application."""
    name = models.CharField('Name', max_length=255, default='')
    right_ascension = models.CharField(
        'Right Ascension', max_length=255, default='')
    declination = models.CharField('Declination', max_length=255, default='')
    vmagnitude = models.FloatField('V Magnitude', default='')
    comment = models.CharField('Comment',
        max_length=255, blank=True, default=None)
    application = models.ForeignKey(Application, related_name='targets')

    def __str__(self):
        return self.name


class OtherApplication(models.Model):
    """A list of applications this quarter at other observatories."""
    telescope = models.CharField('Telescope', max_length=255, default='')
    title = models.CharField('Title', max_length=255, default='')
    application = models.ForeignKey(Application, related_name='other_applications')


class PreviousApplication(models.Model):
    """A list of previous applications at the SAAO."""
    reference = models.CharField('Reference Number', max_length=255, default='')
    time_awarded = models.CharField('Time Awarded', max_length=255)
    useful_percentage = models.CharField(
        'Useful Percentage', max_length=255, default='')
    comments = models.CharField('Comments',
        max_length=255, blank=True, default=None)
    application = models.ForeignKey(Application, related_name='previous_applications')


class Report(models.Model):
    """A light pollution report."""

    class Meta:
        ordering = ['-created_at']

    lat = models.FloatField()
    lng = models.FloatField()
    comment = models.TextField()
    observed_at = models.DateTimeField()
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL)
    created_at = models.DateTimeField(auto_now_add=True)
