from django.conf import settings
from django.conf.urls import include, url
from portal import views


urlpatterns = [
    url(r'^$', views.dashboard, name='dashboard'),
    url(r'^telescopes/(?P<pk>\d+)$', views.telescope, name='telescope'),
    url(r'^instruments/(?P<pk>\d+)$', views.instrument, name='instrument'),
    url(r'^applications/', include('portal.applications.urls', namespace='applications')),
    url(r'^profile/$', views.profile, name='profile'),

    url(r'^admin/', include('portal.admin.urls', namespace='admin')),

    url(r'^admin/maintenance/', include(
        'portal.maintenance.urls', namespace='maintenance')),

    url(r'^', include('portal.auth.urls')),

    url(r'^o/authorize/$', views.AuthView.as_view(), name='authorize'),
    url(r'^o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
    url(r'^api/me$', views.ApiEndpoint.as_view()),
]


if settings.DEBUG:
    from django.views import static
    from django.views.generic.base import TemplateView
    urlpatterns += [
        url(r'^403$', TemplateView.as_view(template_name='403.html')),
        url(r'^404$', TemplateView.as_view(template_name='404.html')),
        url(r'^500$', TemplateView.as_view(template_name='500.html')),
        url(r'^docs/(?P<path>.*)$', static.serve, {
            'document_root': 'docs/_build/html/',
        }),
        url(r'^media/(?P<path>.*)$', static.serve, {
            'document_root': settings.MEDIA_ROOT,
        }),
        url(r'^static/(?P<path>.*)$', static.serve, {
            'document_root': settings.STATIC_ROOT,
        }),
   ]
