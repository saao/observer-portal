import pytz

from django.utils import timezone


class TimezoneMiddleware(object):
    def process_request(self, request):
        if hasattr(request.user, 'profile'):
            timezone.activate(pytz.timezone(request.user.profile.timezone))
        else:
            timezone.deactivate()
