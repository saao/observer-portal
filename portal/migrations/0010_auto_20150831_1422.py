# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('portal', '0009_auto_20150831_1357'),
    ]

    operations = [
        migrations.AlterField(
            model_name='application',
            name='time_bright',
            field=models.PositiveIntegerField(null=True, verbose_name='Bright', blank=True),
        ),
        migrations.AlterField(
            model_name='application',
            name='time_dark',
            field=models.PositiveIntegerField(null=True, verbose_name='Dark', blank=True),
        ),
        migrations.AlterField(
            model_name='application',
            name='time_grey',
            field=models.PositiveIntegerField(null=True, verbose_name='Grey', blank=True),
        ),
        migrations.AlterField(
            model_name='application',
            name='time_min_bright',
            field=models.PositiveIntegerField(null=True, verbose_name='Bright', blank=True),
        ),
        migrations.AlterField(
            model_name='application',
            name='time_min_dark',
            field=models.PositiveIntegerField(null=True, verbose_name='Dark', blank=True),
        ),
        migrations.AlterField(
            model_name='application',
            name='time_min_grey',
            field=models.PositiveIntegerField(null=True, verbose_name='Grey', blank=True),
        ),
    ]
