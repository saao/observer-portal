# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import portal.models


class Migration(migrations.Migration):

    dependencies = [
        ('portal', '0015_auto_20150914_0847'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='avatar',
            field=models.ImageField(default=None, null=True, upload_to=portal.models.get_avatar_name, blank=True),
        ),
    ]
