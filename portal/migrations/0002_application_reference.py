# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('portal', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='application',
            name='reference',
            field=models.CharField(max_length=255, unique=True, null=True, verbose_name='Reference'),
            preserve_default=True,
        ),
    ]
