# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('portal', '0013_auto_20150911_1002'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='previousapplication',
            name='date_awarded',
        ),
        migrations.AddField(
            model_name='previousapplication',
            name='time_awarded',
            field=models.CharField(default='', max_length=255, verbose_name='Time Awarded'),
            preserve_default=False,
        ),
    ]
