# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('portal', '0010_auto_20150831_1422'),
    ]

    operations = [
        migrations.AlterField(
            model_name='application',
            name='email',
            field=models.EmailField(default='', max_length=254, blank=True),
        ),
        migrations.AlterField(
            model_name='application',
            name='first_name',
            field=models.CharField(default='', max_length=255, verbose_name='First Name', blank=True),
        ),
        migrations.AlterField(
            model_name='application',
            name='last_name',
            field=models.CharField(default='', max_length=255, verbose_name='Last Name', blank=True),
        ),
    ]
