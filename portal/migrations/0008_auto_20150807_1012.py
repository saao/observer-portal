# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('portal', '0007_userprofile_timezone'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='applicant',
            name='user',
        ),
        migrations.RemoveField(
            model_name='application',
            name='applicants',
        ),
        migrations.AddField(
            model_name='applicant',
            name='email',
            field=models.EmailField(max_length=254, null=True),
        ),
        migrations.AddField(
            model_name='applicant',
            name='first_name',
            field=models.CharField(default='', max_length=255, verbose_name='First Name'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='applicant',
            name='institution',
            field=models.CharField(default='', max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='applicant',
            name='last_name',
            field=models.CharField(default='', max_length=255, verbose_name='Last Name'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='applicant',
            name='application',
            field=models.ForeignKey(related_name='applicants', to='portal.Application'),
        ),
        migrations.AlterField(
            model_name='application',
            name='email',
            field=models.EmailField(max_length=254),
        ),
    ]
