# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('portal', '0011_auto_20150901_1410'),
    ]

    operations = [
        migrations.AlterField(
            model_name='application',
            name='magnitude_range_max',
            field=models.PositiveIntegerField(default=0, verbose_name='Maximum (Faintest)', blank=True),
        ),
        migrations.AlterField(
            model_name='application',
            name='magnitude_range_min',
            field=models.PositiveIntegerField(default=0, verbose_name='Minimum (Brightest)', blank=True),
        ),
        migrations.AlterField(
            model_name='application',
            name='snr',
            field=models.PositiveIntegerField(default=0, verbose_name='SNR', blank=True),
        ),
        migrations.AlterField(
            model_name='target',
            name='vmagnitude',
            field=models.FloatField(default=0, verbose_name='V Magnitude'),
        ),
    ]
