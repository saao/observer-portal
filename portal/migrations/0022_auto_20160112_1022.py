# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('portal', '0021_userprofile_avatar'),
    ]

    operations = [
        migrations.AlterField(
            model_name='application',
            name='magnitude_range_max',
            field=models.FloatField(default=0, null=True, verbose_name='Maximum (Faintest)', blank=True),
        ),
        migrations.AlterField(
            model_name='application',
            name='magnitude_range_min',
            field=models.FloatField(default=0, null=True, verbose_name='Minimum (Brightest)', blank=True),
        ),
        migrations.AlterField(
            model_name='application',
            name='time_additional_bright',
            field=models.FloatField(default=0, verbose_name='Bright', blank=True),
        ),
        migrations.AlterField(
            model_name='application',
            name='time_additional_dark',
            field=models.FloatField(default=0, verbose_name='Dark', blank=True),
        ),
        migrations.AlterField(
            model_name='application',
            name='time_additional_grey',
            field=models.FloatField(default=0, verbose_name='Grey', blank=True),
        ),
        migrations.AlterField(
            model_name='application',
            name='time_bright',
            field=models.FloatField(null=True, verbose_name='Bright', blank=True),
        ),
        migrations.AlterField(
            model_name='application',
            name='time_dark',
            field=models.FloatField(null=True, verbose_name='Dark', blank=True),
        ),
        migrations.AlterField(
            model_name='application',
            name='time_grey',
            field=models.FloatField(null=True, verbose_name='Grey', blank=True),
        ),
        migrations.AlterField(
            model_name='application',
            name='time_min_bright',
            field=models.FloatField(null=True, verbose_name='Bright', blank=True),
        ),
        migrations.AlterField(
            model_name='application',
            name='time_min_dark',
            field=models.FloatField(null=True, verbose_name='Dark', blank=True),
        ),
        migrations.AlterField(
            model_name='application',
            name='time_min_grey',
            field=models.FloatField(null=True, verbose_name='Grey', blank=True),
        ),
    ]
