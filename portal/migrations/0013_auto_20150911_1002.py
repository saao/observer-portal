# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('portal', '0012_auto_20150911_0914'),
    ]

    operations = [
        migrations.AlterField(
            model_name='target',
            name='vmagnitude',
            field=models.FloatField(default=0, verbose_name='V Magnitude'),
        ),
    ]
