# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-06-29 11:46
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('portal', '0025_auto_20160628_1358'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='log',
            name='role',
        ),
    ]
