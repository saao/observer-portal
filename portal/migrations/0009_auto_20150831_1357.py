# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('portal', '0008_auto_20150807_1012'),
    ]

    operations = [
        migrations.AlterField(
            model_name='application',
            name='title',
            field=models.CharField(default='Untitled', max_length=255, verbose_name='Title'),
        ),
    ]
