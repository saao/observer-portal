# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('portal', '0004_auto_20150730_0848'),
    ]

    operations = [
        migrations.AlterField(
            model_name='previousapplication',
            name='comments',
            field=models.CharField(default=None, max_length=255, verbose_name='Comments', blank=True),
        ),
        migrations.AlterField(
            model_name='target',
            name='comment',
            field=models.CharField(default=None, max_length=255, verbose_name='Comment', blank=True),
        ),
    ]
