# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-06-28 11:58
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('portal', '0023_auto_20160122_0832'),
    ]

    operations = [
        migrations.AddField(
            model_name='log',
            name='standby',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='userprofile',
            name='department',
            field=models.CharField(blank=True, choices=[('IT', 'IT'), ('Electronics', 'Electronics'), ('Mechanical', 'Mechanical')], default=None, max_length=255, null=True),
        ),
    ]
