from __future__ import unicode_literals

from django.core import validators
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin


class UserManager(BaseUserManager):
    def create_user(self, email, password=None):
        if not email:
            raise ValueError('Users must have an email address')
        user = self.model(email=self.normalize_email(email),)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        user = self.create_user(email, password=password)
        user.is_superuser = True
        user.is_staff = True
        user.is_active = True
        user.save(using=self._db)
        return user


@python_2_unicode_compatible
class User(AbstractBaseUser, PermissionsMixin):

    class Meta:
        ordering = ['first_name', 'last_name', 'email']

    # This is a requirement imposed by our LDAP configuration. The username
    # maps to the uid in LDAP which will be used to create *nix accounts for
    # people to log in with and download their data.
    username = models.CharField(
        max_length=30,
        unique=True,
        null=True,
        validators=[
            validators.RegexValidator(r'^[a-z_][a-z0-9-_]*$',
                'Username must start with a lowercase letter or underscore followed by '
                  'lowercase letters, numbers, hyphens and underscores.'
            ),
        ],
        error_messages={
            'unique': 'A user with that username already exists.',
        },
    )
    email = models.EmailField(unique=True)

    first_name = models.CharField(max_length=32, blank=True, default='')
    last_name = models.CharField(max_length=32, blank=True, default='')

    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'email'

    def __str__(self):
        if not self.first_name and not self.last_name:
            return self.email
        return ' '.join([self.first_name, self.last_name])
