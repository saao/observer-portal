from django.contrib.auth import get_user_model
from django.contrib.auth.tokens import default_token_generator
from django.views.decorators.debug import sensitive_post_parameters
from django.views.decorators.cache import never_cache
from django.utils.http import urlsafe_base64_decode
from django.shortcuts import render, redirect
from .forms import UserRegistrationForm


@sensitive_post_parameters('password1', 'password2')
def register(request):
    if request.method == 'POST':
        form = UserRegistrationForm(data=request.POST)
        if form.is_valid():
            user = form.save(request=request, use_https=request.is_secure())
            return redirect('/activate/')
    else:
        form = UserRegistrationForm()
    return render(request, 'auth/register.html', {'form': form})


@never_cache
def activate(request, uidb64=None, token=None):

    if uidb64 is None or token is None:
        return render(request, 'auth/activation.html')

    UserModel = get_user_model()
    try:
        uid = urlsafe_base64_decode(uidb64)
        user = UserModel._default_manager.get(pk=uid)
    except (TypeError, ValueError, OverflowError, UserModel.DoesNotExist):
        user = None

    if user is not None and default_token_generator.check_token(user, token):
        user.is_active = True
        user.save()
        return render(request, 'auth/activated.html')
    else:
        return render(request, 'auth/not_activated.html')
