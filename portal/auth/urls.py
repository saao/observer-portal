from django.conf.urls import url
from django.contrib.auth import views
from .views import register, activate
from .forms import LoginForm


urlpatterns = [
    url(r'^register/$', register, name='register'),
    url(r'^activate/$', activate, name='activate'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        activate, name='activate'),
    url(r'^login/$', views.login,
        {'template_name': 'auth/login.html', 'authentication_form': LoginForm}, name='login'),
    url(r'^logout/$', views.logout, {'next_page': '/'}, name='logout'),

    url(r'^password_reset/$', views.password_reset,
        {'template_name': 'auth/password_reset_form.html',
            'email_template_name': 'auth/emails/password_reset.txt',
            'subject_template_name': 'auth/emails/password_reset_subject.txt'}, name='password_reset'),
    url(r'^password_reset/done/$', views.password_reset_done,
        {'template_name': 'auth/password_reset_done.html'}, name='password_reset_done'),
    url(r'^password_reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views.password_reset_confirm,
        {'template_name': 'auth/password_reset_confirm.html'}, name='password_reset_confirm'),
    url(r'^password_reset/complete/$', views.password_reset_complete,
        {'template_name': 'auth/password_reset_complete.html'}, name='password_reset_complete'),
]
