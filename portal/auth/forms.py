from __future__ import unicode_literals

import ldap3

from django import forms
from django.conf import settings
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.core.mail import send_mail
from django.contrib.auth import get_user_model
from django.contrib.auth.tokens import default_token_generator
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.sites.shortcuts import get_current_site
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes
from django.template.loader import render_to_string


User = get_user_model()


class LoginForm(AuthenticationForm):
    username = forms.EmailField(widget=forms.EmailInput(
        attrs={'placeholder': 'E-mail address', 'class': 'form-control', 'autofocus': True}))
    password = forms.CharField(widget=forms.PasswordInput(
        attrs={'placeholder': 'Password', 'class': 'form-control', 'autocomplete': 'current-password'}))


class UserForm(forms.ModelForm):
    email = forms.EmailField(widget=forms.EmailInput(
        attrs={'placeholder': 'E-mail address', 'class': 'form-control'}))
    password1 = forms.CharField(widget=forms.PasswordInput(
        attrs={'placeholder': 'Password', 'class': 'form-control', 'autocomplete': 'new-password'}))
    password2 = forms.CharField(widget=forms.PasswordInput(
        attrs={'placeholder': 'Repeat password', 'class': 'form-control'}))

    class Meta:
        model = User
        fields = ('email', 'password1', 'password2')

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 != password2:
            raise forms.ValidationError('Passwords do not match.')
        return password2

    def clean_email(self):
        email = self.cleaned_data.get('email')
        try:
            User._default_manager.get(email=email)
        except User.DoesNotExist:
            server = ldap3.Server(settings.LDAP_HOST)
            with ldap3.Connection(server) as c:
                if not c.search('ou=people,dc=saao',
                        '(mail={})'.format(email), size_limit=1):
                    return email

        raise forms.ValidationError('This e-mail is already in use.')

    def save(self, commit=True, request=None):
        user = super(UserForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password1'])
        user.save()
        return user


class UserRegistrationForm(UserForm):

    username = forms.CharField(widget=forms.TextInput(
        attrs={'placeholder': 'Username', 'class': 'form-control', 'autofocus': True}))

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2')

    def clean_username(self):
        username = self.cleaned_data.get('username')
        server = ldap3.Server(settings.LDAP_HOST)
        with ldap3.Connection(server) as c:
            if not c.search('ou=people,dc=saao',
                    '(uid={})'.format(username), size_limit=1):
                return username
        raise forms.ValidationError('This username is already in use.')

    def save(self, request=None, use_https=False):
        user = super(UserRegistrationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password1'])
        user.save()
        context = {
            'email': user.email,
            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
            'user': user,
            'domain': get_current_site(request).domain,
            'token': default_token_generator.make_token(user),
            'protocol': 'https' if use_https else 'http',
        }

        subject = '[SAAO] Welcome to the observer portal'
        message = render_to_string('auth/emails/activation.txt', context)
        send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [user.email])

        return user


class UserChangeForm(forms.ModelForm):
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = User
        fields = ('email', 'password', 'first_name', 'last_name', 'is_active')

    def clean_password(self):
        return self.initial['password']
