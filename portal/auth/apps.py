"""Define a unique name and label for the app that doesn't clash with Django's auth app."""
from django.apps import AppConfig


class AuthConfig(AppConfig):
    name = 'portal.auth'
    label = 'portal_auth'
