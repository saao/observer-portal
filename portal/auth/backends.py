from ldap3 import Server, Connection, ALL
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend
from django.core.files.base import ContentFile


LDAP_HOST = getattr(settings, 'LDAP_HOST', 'localhost')
LDAP_SEARCH_BASE = getattr(settings, 'LDAP_SEARCH_BASE', 'ou=people,dc=saao')
LDAP_SEARCH_FILTER = getattr(settings, 'LDAP_SEARCH_FILTER', '(mail={})')


class LDAPBackend(ModelBackend):
    def __init__(self, *args, **kwargs):
        super(LDAPBackend, self).__init__(*args, **kwargs)
        self.server = Server(host=LDAP_HOST, get_info=ALL)

    def _get_user_by_email(self, email):
        with Connection(self.server) as c:
            if c.search(LDAP_SEARCH_BASE, LDAP_SEARCH_FILTER.format(email), attributes=[
                    'uid', 'givenName', 'sn', 'jpegPhoto', 'employeeType'], size_limit=1):
                return c.entries[0].entry_get_attributes_dict()

    def authenticate(self, username=None, password=None, **kwargs):
        User = get_user_model()

        entry = self._get_user_by_email(email=username)
        if not entry:
            return

        with Connection(self.server,
                user='uid={},ou=people,dc=saao'.format(entry['uid'][0]), password=password) as c:
            if c.bind():
                user, created = User.objects.get_or_create(email=username, defaults={
                    'username': str(entry['uid'][0]).strip(),
                    'first_name': entry.get('givenName', [''])[0].strip(),
                    'last_name': entry.get('sn', [''])[0].strip(),
                    'is_staff': str(entry.get('employeeType', [''])[0]).strip() == 'staff',
                })

                # If we just created the user it means that they already had an
                # entry in LDAP. So, in this case it should be safe to assume
                # that they can be activated.
                if created:
                    if 'jpegPhoto' in entry:
                        user.profile.avatar.save(
                            '{}.jpg'.format(user.pk), ContentFile(entry['jpegPhoto'][0]))
                    user.is_active = True
                    user.save()

                return user
