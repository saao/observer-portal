from __future__ import unicode_literals

import mock

from django.test import TransactionTestCase
from django.conf import settings
from django.core import mail
from django.core.urlresolvers import reverse
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes
from django.contrib.auth.tokens import default_token_generator
from .forms import LoginForm, UserRegistrationForm
from .models import User


class LoginFormTests(TransactionTestCase):
    fixtures = ['portal/fixtures/initial_data.json']

    def test_blank_data(self):
        form = LoginForm(data={})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {
            'username': ['This field is required.'],
            'password': ['This field is required.'],
        })

    @mock.patch('ldap3.Connection')
    def test_invalid_data(self, connection):
        form = LoginForm(data={
            'username': 'username',
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {
            'username': ['Enter a valid email address.'],
            'password': ['This field is required.'],
        })

    @mock.patch('ldap3.Connection')
    def test_valid_data(self, connection):
        form = LoginForm(data={
            'username': 'username@example.com',
            'password': 'password',
        })
        self.assertTrue(form.is_valid())

    @mock.patch('ldap3.Connection')
    def test_inactive_user(self, connection):
        user = User.objects.create(
            username='username', email='username@example.com')
        user.set_password('password')
        user.save()
        form = LoginForm(data={
            'username': 'username@example.com',
            'password': 'password',
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {'__all__': ['This account is inactive.']})

    @mock.patch('ldap3.Connection')
    def test_active_user(self, connection):
        user = User.objects.create(
            username='username', email='username@example.com', is_active=True)
        user.set_password('password')
        user.save()
        form = LoginForm(data={
            'username': 'username@example.com',
            'password': 'password',
        })
        self.assertTrue(form.is_valid())
        self.assertEqual(form.errors, {})


class RegistrationFormTests(TransactionTestCase):
    fixtures = ['portal/fixtures/initial_data.json']

    def test_blank_data(self):
        form = UserRegistrationForm(data={})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {
            'username': ['This field is required.'],
            'password1': ['This field is required.'],
            'password2': ['This field is required.'],
            'email': ['This field is required.'],
        })

    @mock.patch('ldap3.Connection', return_value=mock.MagicMock())
    def test_invalid_data(self, connection):
        # Always return False when searching for users (the user doesn't exist).
        connection.return_value.__enter__.return_value.search = mock.Mock(
            return_value=False)

        form = UserRegistrationForm(data={
            'username': '1invalid',
            'password1': 'foo',
            'password2': 'bar',
            'email': 'notanemail',
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {
            'username': ['Username must start with a lowercase letter or underscore followed by lowercase letters, numbers, hyphens and underscores.'],
            'password2': ['Passwords do not match.'],
            'email': ['Enter a valid email address.'],
        })

    @mock.patch('ldap3.Connection')
    def test_valid_data(self, connection):
        # Always return False when searching for users (the user doesn't exist).
        connection.return_value.__enter__.return_value.search = mock.Mock(
            return_value=False)

        form = UserRegistrationForm(data={
            'username': 'username',
            'password1': 'password',
            'password2': 'password',
            'email': 'username@example.com',
        })
        self.assertTrue(form.is_valid())
        self.assertEqual(form.errors, {})

    @mock.patch('ldap3.Connection')
    def test_existing_user(self, connection):
        # Always return False when searching for users (the user doesn't exist).
        connection.return_value.__enter__.return_value.search = mock.Mock(
            return_value=False)

        user = User.objects.create(
            username='username', email='username@example.com', is_active=True)
        user.set_password('password')
        user.save()
        form = UserRegistrationForm(data={
            'username': 'username',
            'password1': 'password',
            'password2': 'password',
            'email': 'username@example.com',
        })
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors, {
            'username': ['A user with that username already exists.'],
            'email': ['This e-mail is already in use.'],
        })


class ViewTests(TransactionTestCase):
    fixtures = ['portal/fixtures/initial_data.json']

    def test_signin_invalid_data(self):
        response = self.client.get(reverse('login'))
        self.assertTrue(response.status_code==200)

        response = self.client.post(reverse('login'), {
            'username': '',
            'password': '',
        }, follow=True)

        # The view is rendered again with no redirect.
        self.assertTrue(response.status_code==200)
        self.assertFormError(response, 'form', 'username', 'This field is required.')
        self.assertFormError(response, 'form', 'password', 'This field is required.')

    def test_signup_invalid_data(self):
        response = self.client.get(reverse('register'))
        self.assertTrue(response.status_code==200)

        response = self.client.post(reverse('register'), {
            'username': '',
            'email': '',
            'password1': '',
            'password2': '',
        }, follow=True)

        # The view is rendered again with no redirect.
        self.assertTrue(response.status_code==200)
        self.assertFormError(response, 'form', 'username', 'This field is required.')
        self.assertFormError(response, 'form', 'email', 'This field is required.')
        self.assertFormError(response, 'form', 'password1', 'This field is required.')
        self.assertFormError(response, 'form', 'password2', 'This field is required.')

    def test_signin_inactive(self):
        user = User.objects.create(
            username='username', email='username@example.com')
        user.set_password('password')
        user.save()

        response = self.client.post(reverse('login'), {
            'username': 'username@example.com',
            'password': 'password',
        }, follow=True)

        self.assertContains(response, 'This account is inactive.')
        self.assertFormError(response, 'form', None, 'This account is inactive.')

    def test_activate_invalid_token(self):
        response = self.client.get(reverse('activate', kwargs={
            'uidb64': 'FFF',
            'token': 'FFF-FFF',
        }))
        # We still receive a 'success' response with invalid parameters to
        # prevent information leakage.
        self.assertTrue(response.status_code==200)

    def test_activate_valid_token(self):
        user = User.objects.create(
            username='username', email='username@example.com')
        user.set_password('password')
        user.save()

        self.assertTrue(user.is_active==False)

        uidb64 = urlsafe_base64_encode(force_bytes(user.pk))
        token = default_token_generator.make_token(user)

        response = self.client.get(reverse('activate', kwargs={
            'uidb64': uidb64,
            'token': token,
        }))
        self.assertTrue(response.status_code==200)

        user.refresh_from_db()

        self.assertTrue(user.is_active==True)

    @mock.patch('ldap3.Connection')
    def test_signup(self, connection):
        # Always return False when searching for users (the user doesn't exist).
        connection.return_value.__enter__.return_value.search = mock.Mock(
            return_value=False)

        response = self.client.post(reverse('register'), {
            'username': 'username',
            'email': 'username@example.com',
            'password1': 'password',
            'password2': 'password',
        }, follow=True)

        self.assertRedirects(
            response, reverse('activate'), target_status_code=200)

    def test_signin(self):
        user = User.objects.create(
            username='username', email='username@example.com', is_active=True)
        user.set_password('password')
        user.save()

        response = self.client.post(reverse('login'), {
            'username': 'username@example.com',
            'password': 'password',
        }, follow=True)

        self.assertRedirects(
            response, settings.LOGIN_REDIRECT_URL, target_status_code=200)

    def test_signout(self):
        response = self.client.get(reverse('logout'), follow=True)
        self.assertRedirects(response, settings.LOGIN_REDIRECT_URL)

    def test_reset_password(self):
        response = self.client.get(reverse('password_reset'))
        self.assertTrue(response.status_code==200)

        user = User.objects.create(
            username='username', email='username@example.com', is_active=True)
        user.set_password('password')
        user.save()

        response = self.client.post(reverse('password_reset'), {
            'email': 'username@example.com',
        })
        self.assertRedirects(response, reverse('password_reset_done'))

        self.assertEqual(len(mail.outbox), 1)
        self.assertTrue(user.email in mail.outbox[0].to)
