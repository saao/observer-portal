from django import forms
from django.forms.models import BaseInlineFormSet
from portal.models import Applicant, Telescope, Instrument


class RequiredFormset(BaseInlineFormSet):
    """Makes sure that at least one entry is supplied in a formset."""

    def __init__(self, *args, **kwargs):
        super(RequiredFormset, self).__init__(*args, **kwargs)
        try:
            self.forms[0].empty_permitted = False
        except IndexError:
            pass


class ApplicantForm(forms.ModelForm):
    class Meta:
        model = Applicant
        fields = ('first_name', 'last_name', 'email', 'institution', 'role',
            'is_observer')
        widgets = {
            'first_name': forms.TextInput(
                attrs={'class': 'form-control', 'placeholder': 'First name'}),
            'last_name': forms.TextInput(
                attrs={'class': 'form-control', 'placeholder': 'Last name'}),
            'email': forms.EmailInput(
                attrs={'class': 'form-control', 'placeholder': 'Email'}),
            'institution': forms.TextInput(
                attrs={'class': 'form-control', 'placeholder': 'Institution'}),
            'role': forms.Select(attrs={'class': 'form-control'}),
        }


class BaseApplicationForm(forms.ModelForm):
    # Don't add 'required' attribute to input fields.
    use_required_attribute = False

    telescope = forms.ModelChoiceField(
        widget=forms.Select(attrs={'class': 'form-control'}),
        queryset=Telescope.objects.none(),
        empty_label='')

    instrument = forms.ModelChoiceField(
        widget=forms.Select(attrs={'class': 'form-control'}),
        queryset=Instrument.objects.none(),
        empty_label='')

    def __init__(self, data=None, *args, **kwargs):
        super(BaseApplicationForm, self).__init__(data, *args, **kwargs)

        # Set the instrument queryset if we received an application instance.
        instance = kwargs.get('instance')
        if instance and instance.telescope:
            self.fields['instrument'].queryset = Instrument.objects.filter(
                telescope=instance.telescope, is_available=True)

    def is_valid(self):
        valid = super(BaseApplicationForm, self).is_valid()
        for formset in self.formsets.values():
            valid = valid and formset.is_valid()
        return valid

    def clean_scientific_justification(self):
        import magic
        f = self.cleaned_data.get('scientific_justification', False)
        if f:

            # Validate document type.
            if 'PDF' not in magic.from_buffer(f.read()).decode('utf-8'):
                raise forms.ValidationError('The document must be a PDF.')

            # Validate document size.
            limit = 1024 * 1024 * 5
            if f.size > limit:
                raise forms.ValidationError('The document should not be bigger than 5MB.')

            return f

    def save(self, created_by=None):
        application = super(BaseApplicationForm, self).save(commit=False)
        if created_by:
            application.created_by = created_by
        application.save()

        for formset in self.formsets.values():
            models = formset.save(commit=False)
            for model in models:
                model.application = application
                model.save()

            for obj in formset.deleted_objects:
                obj.delete()

        return application

    def submit(self):
        application = super(BaseApplicationForm, self).save(commit=False)
        application.submit()
        application.save()

        return application
