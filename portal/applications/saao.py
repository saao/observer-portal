# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import os
import datetime

from io import BytesIO
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib import colors
from reportlab.lib.units import cm
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.platypus import (
    SimpleDocTemplate, Paragraph, Spacer, Table, TableStyle)
from PyPDF2 import PdfFileReader, PdfFileWriter
from django import forms
from django.conf import settings
from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.forms.models import inlineformset_factory
from django.template.loader import render_to_string
from django_fsm import transition
from portal.models import (Target, Application, Applicant, OtherApplication,
    PreviousApplication)
from portal.applications.forms import (
    RequiredFormset, ApplicantForm, BaseApplicationForm)


pdfmetrics.registerFont(TTFont('NotoSans',
    os.path.join(settings.STATIC_ROOT, 'fonts', 'NotoSans-unhinted',
        'NotoSans-Regular.ttf')))
pdfmetrics.registerFont(TTFont('NotoSansBold',
    os.path.join(settings.STATIC_ROOT, 'fonts', 'NotoSans-unhinted',
        'NotoSans-Bold.ttf')))
pdfmetrics.registerFont(TTFont('NotoSansItalic',
    os.path.join(settings.STATIC_ROOT, 'fonts', 'NotoSans-unhinted',
        'NotoSans-Italic.ttf')))
pdfmetrics.registerFont(TTFont('NotoSansBoldItalic',
    os.path.join(settings.STATIC_ROOT, 'fonts', 'NotoSans-unhinted',
        'NotoSans-BoldItalic.ttf')))


class SAAOApplication(Application):
    class Meta:
        proxy = True

    def as_pdf(self):
        path = os.path.join(
            settings.MEDIA_ROOT, 'applications', '{}.pdf'.format(
                self.reference)).encode('utf-8')

        if os.path.exists(path) and self.state != 'draft':
            # A small workaround for when we've lost the original scientific
            # justification document, but we have stored the complete
            # application previously.
            with open(path) as f:
                buffer = BytesIO(f.read())
        else:
            styles = getSampleStyleSheet()

            styles['BodyText'].fontName = 'NotoSans'

            h1 = styles['Heading1']
            h1.alignment = 1
            h1.fontSize = 14

            h2 = styles['Heading2']
            h2.fontSize = 11
            h2.leading = 20

            h3 = styles['Heading3']
            h3.fontName = 'Helvetica'
            h3.fontSize = 10

            paragraph = styles['Normal']
            paragraph.fontSize = 9
            paragraph.fontName = 'NotoSans'

            table_style = TableStyle([
                ('LEFTPADDING', (0,0), (-1,-1), 0),
                ('RIGHTLEFTPADDING', (0,0), (-1,-1), 0),
                ('FONTSIZE', (0,0), (-1,-1), 9),
                ('FONTNAME', (0,0), (-1,-1), 'NotoSans'),
                ('LINEABOVE', (0,0), (-1,0), 2, colors.black),
                ('LINEABOVE', (0,1), (-1,-1), 0.25, colors.grey),
                ('VALIGN', (0,0), (-1,-1), 'TOP')])

            story = []

            story.append(Paragraph('Telescope Time Applications', h1))

            title = h1
            title.alignment = 1
            title.fontSize = 13
            title.leading = 12
            title.spaceBefore = 20
            title.spaceAfter = 20

            story.append(Paragraph(self.title, title))

            applicants = self.applicants.all()
            if applicants:
                story.append(Paragraph('Applicants', h2))
                data = [
                    [Paragraph('<b>Name</b>', h3),
                        Paragraph('<b>Institution</b>', h3),
                        Paragraph('<b>Role</b>', h3),
                        Paragraph('<b>Observer?</b>', h3)]
                ]
                for applicant in applicants:
                    data.append([
                        applicant,
                        applicant.institution,
                        applicant.get_role_display(),
                        ('Yes' if applicant.is_observer else 'No')
                    ])
                story.append(Table(data, style=table_style))

            story.append(Paragraph('Principal Contact', h2))
            data = [
                [Paragraph('<b>Name</b>', h3),
                    Paragraph('<b>Phone</b>', h3),
                    Paragraph('<b>Email</b>', h3)],
                ['{} {}'.format(self.first_name, self.last_name),
                    self.phone,
                    self.email],
                [Paragraph('<b>Postal Address</b', h3)],
                [Paragraph(self.postal_address, paragraph)],
                [Paragraph('<b>City</b>', h3),
                    Paragraph('<b>Zip / Postal Code</b>', h3),
                    Paragraph('<b>Country</b>', h3)],
                [self.city, self.postal_code,
                        self.get_country_display()]
            ]
            custom_table_style = table_style.getCommands()+[('SPAN',(0,3),(2,3),)]
            table = Table(data, style=custom_table_style)
            story.append(table)

            story.append(Paragraph('Abstract', h2))
            data = [
                [Paragraph(self.abstract.replace('\n\n', '<br/><br/>'), paragraph)],
            ]
            story.append(Table(data, style=table_style))

            story.append(Paragraph('Summary for the General Public', h2))
            data = [
                [Paragraph(self.abstract_public.replace('\n\n', '<br/><br/>'), paragraph)],
            ]
            story.append(Table(data, style=table_style))
            story.append(Spacer(1, 0.5*cm))

            # Telescope, Instrument, Gratings/Filters and SNR
            data = [
                [Paragraph('<b>Telescope</b>', h3), Paragraph('<b>Instrument</b>', h3),
                    Paragraph('<b>Gratings and Filters</b>', h3), Paragraph('<b>SNR</b>', h3)],
                ['None' if not self.telescope else self.telescope.name,
                    'None' if not self.instrument else self.instrument.name,
                    'None' if not self.gratings_filters else self.gratings_filters,
                    'None' if not self.snr else self.snr]
            ]
            story.append(Table(data, style=table_style))

            story.append(Paragraph('Magnitude Range', h2))
            data = [
                [Paragraph('<b>Min (Brightest)</b>', h3),
                    Paragraph('<b>Max (Faintest)</b>', h3),
                    Paragraph('<b>Filter</b>', h3)],
                [self.magnitude_range_min or 'None',
                    self.magnitude_range_max or 'None',
                    self.magnitude_range_filter or 'None']
            ]
            story.append(Table(data, style=table_style))

            story.append(Paragraph('Requested Time', h2))
            data = [
                [Paragraph('<b>Dark</b>', h3),
                    Paragraph('<b>Grey</b>', h3),
                    Paragraph('<b>Bright</b>', h3)],
                [self.time_dark,
                    self.time_grey,
                    self.time_bright]
            ]
            story.append(Table(data, style=table_style))

            story.append(Paragraph('Minimum Useful Time', h2))
            data = [
                [Paragraph('<b>Dark</b>', h3),
                    Paragraph('<b>Grey</b>', h3),
                    Paragraph('<b>Bright</b>', h3)],
                [self.time_min_dark,
                    self.time_min_grey,
                    self.time_min_bright]
            ]
            story.append(Table(data, style=table_style))

            story.append(Paragraph('Additional Time', h2))
            data = [
                [Paragraph('<b>Dark</b>', h3),
                    Paragraph('<b>Grey</b>', h3),
                    Paragraph('<b>Bright</b>', h3)],
                [self.time_additional_dark,
                    self.time_additional_grey,
                    self.time_additional_bright]
            ]
            story.append(Table(data, style=table_style))

            story.append(Paragraph('Preferred Moon Quarter Grey Time', h2))
            data = [
                [Paragraph(str(self.get_time_grey_moon_quarter_display()), paragraph)],
            ]
            story.append(Table(data, style=table_style))

            story.append(Paragraph('Preferred Dates', h2))
            if self.dates_preferred:
                dates_preferred = self.dates_preferred.replace('\n\n', '<br/><br/>')
            else:
                dates_preferred = 'None'
            data = [
                [Paragraph(dates_preferred, paragraph)],
            ]
            story.append(Table(data, style=table_style))

            story.append(Paragraph('Impossible Dates', h2))
            if self.dates_impossible:
                dates_impossible = self.dates_impossible.replace('\n\n', '<br/><br/>')
            else:
                dates_impossible = 'None'
            data = [
                [Paragraph(dates_impossible, paragraph)],
            ]
            story.append(Table(data, style=table_style))

            if self.dates_impossible_justification:
                story.append(Paragraph('Justification for Impossible Dates', h2))
                data = [
                    [Paragraph(self.dates_impossible_justification.replace('\n\n', '<br/><br/>'), paragraph)],
                ]
                story.append(Table(data, style=table_style))

            story.append(Paragraph('Other Time Constraints', h2))
            if self.time_constraints_other:
                time_constraints_other = self.time_constraints_other.replace('\n\n', '<br/><br/>')
            else:
                time_constraints_other = 'None'
            data = [
                [Paragraph(time_constraints_other, paragraph)],
            ]
            story.append(Table(data, style=table_style))

            story.append(Paragraph('Required Support', h2))
            data = [
                [Paragraph(self.required_support.replace('\n\n', '<br/><br/>'), paragraph)],
            ]
            story.append(Table(data, style=table_style))

            targets = self.targets.all()
            if targets:
                story.append(Paragraph('Targets', h2))
                data = [
                    [Paragraph('<b>Name</b>', h3),
                        Paragraph('<b>RA</b>', h3),
                        Paragraph('<b>Dec</b>', h3),
                        Paragraph('<b>V Magnitude</b>', h3),
                        Paragraph('<b>Comment</b>', h3)]
                ]
                for target in targets:
                    data.append([
                        target.name,
                        target.right_ascension,
                        target.declination,
                        target.vmagnitude,
                        Paragraph(target.comment, paragraph) or 'None'
                    ])
                story.append(Table(data, style=table_style))

            other_applications = self.other_applications.all()
            if other_applications:
                story.append(Paragraph('Other Applications', h2))
                data = [
                    [Paragraph('<b>Telescope</b>', h3),
                        Paragraph('<b>Title</b>', h3)]
                ]
                for other_application in other_applications:
                    data.append([
                        Paragraph(other_application.telescope, paragraph),
                        Paragraph(other_application.title, paragraph)
                    ])
                story.append(Table(data, style=table_style))

            previous_applications = self.previous_applications.all()
            if previous_applications:
                story.append(Paragraph('Previous Applications', h2))
                data = [
                    [Paragraph('<b>Reference</b>', h3),
                        Paragraph('<b>Date Awarded</b>', h3),
                        Paragraph('<b>Useful Percentage</b>', h3),
                        Paragraph('<b>Comment</b>', h3)]
                ]
                for previous_application in previous_applications:
                    data.append([
                        previous_application.reference,
                        previous_application.time_awarded,
                        previous_application.useful_percentage,
                        Paragraph(previous_application.comments, paragraph) or 'None'
                    ])
                story.append(Table(data, style=table_style))

            buffer = BytesIO()

            doc = SimpleDocTemplate(
                buffer, leftMargin=25, rightMargin=25, topMargin=25, bottomMargin=25)
            doc.title = self.title
            doc.author = str(self.created_by)
            doc.build(story)

            output = PdfFileWriter()
            docs = [buffer]
            if self.scientific_justification:
                docs.append(open(self.scientific_justification.file.name, 'rb'))
            for doc in docs:
                reader = PdfFileReader(doc)
                for page in xrange(reader.numPages):
                    output.addPage(reader.getPage(page))

            buffer = BytesIO()
            output.write(buffer)

            # Save the generated document for later.
            with open(path, 'wb+') as f:
                f.write(buffer.getvalue())

        return buffer

    @transition(field='state', source='draft', target='submitted', permission='portal.submit_application')
    def submit(self):
        # Only set the reference if it hasn't been set already. This keeps the
        # reference the same for resubmitted applications.
        if not self.reference:
            self.reference = '-'.join([self.last_name.replace(' ', '-'),
                datetime.datetime.utcnow().strftime('%Y-%m'),
                self.telescope.name.replace(' ', '-'), str(self.id)])

        attachments = [('{}.pdf'.format(
            self.reference), self.as_pdf().getvalue(), 'application/pdf')]
        message = render_to_string('applications/saao/emails/submitted.txt', {
            'application': self
        })
        email = EmailMessage(
            '[{}] Your Telescope Time Application'.format(self.facility),
            message, settings.DEFAULT_FROM_EMAIL, [self.email],
            reply_to=['observer@saao.ac.za'], attachments=attachments)
        # Include the submitter if they are not the same as the principal contact.
        if self.email != self.created_by.email:
            email.cc = [self.created_by.email]
        email.send()

        text = render_to_string('applications/saao/emails/submitted_notification.txt', {
            'application': self,
        })
        html = render_to_string('applications/saao/emails/submitted_notification.html', {
            'application': self,
        })

        email = EmailMultiAlternatives(
            '[{}] New Telescope Time Application'.format(self.facility),
            text, settings.DEFAULT_FROM_EMAIL, [email for name, email in settings.MANAGERS],
            reply_to=[self.email], attachments=attachments)
        email.attach_alternative(html, 'text/html')
        email.send()


class BaseApplicantFormset(RequiredFormset):
    def clean(self):
        if any(self.errors):
            return

        emails = []
        has_observer = False
        for form in self.forms:
            if not form.cleaned_data or form.cleaned_data['DELETE']:
                continue

            email = form.cleaned_data['email']
            if email in emails:
                raise forms.ValidationError(
                    'Each applicant must have a unique email address.')
            emails.append(email)

            has_observer = has_observer or (
                'is_observer' in form.cleaned_data and form.cleaned_data['is_observer'])

        if not has_observer:
            raise forms.ValidationError('Please select at least one observer.')


class ApplicationForm(BaseApplicationForm):
    class Meta:
        model = SAAOApplication
        exclude = ['reference', 'created_by', 'state', 'call']
        labels = {
            'postal_code': 'Zip / Postal Code',
        }
        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'form-control'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control'}),
            'email': forms.EmailInput(attrs={'class': 'form-control'}),
            'title': forms.TextInput(attrs={'class': 'form-control'}),
            'abstract': forms.Textarea(attrs={'class': 'form-control'}),
            'abstract_public': forms.Textarea(attrs={'class': 'form-control'}),
            'street': forms.TextInput(attrs={'class': 'form-control'}),
            'street2': forms.TextInput(attrs={'class': 'form-control'}),
            'city': forms.TextInput(attrs={'class': 'form-control'}),
            'postal_code': forms.TextInput(attrs={'class': 'form-control'}),
            'country': forms.Select(attrs={'class': 'form-control'}),
            'phone': forms.TextInput(attrs={'class': 'form-control'}),

            'gratings_filters': forms.TextInput(
                attrs={
                    'placeholder': 'e.g. UBVRI, Hα, Stromgren, grating 7, etc.',
                    'class': 'form-control'}),
            'snr': forms.TextInput(attrs={'class': 'form-control'}),

            'magnitude_range_min': forms.TextInput(attrs={'class': 'form-control'}),
            'magnitude_range_max': forms.TextInput(attrs={'class': 'form-control'}),
            'magnitude_range_filter': forms.TextInput(attrs={'class': 'form-control'}),

            'time_dark': forms.TextInput(attrs={'class': 'form-control'}),
            'time_grey': forms.TextInput(attrs={'class': 'form-control'}),
            'time_bright': forms.TextInput(attrs={'class': 'form-control'}),

            'time_min_dark': forms.TextInput(attrs={'class': 'form-control'}),
            'time_min_grey': forms.TextInput(attrs={'class': 'form-control'}),
            'time_min_bright': forms.TextInput(attrs={'class': 'form-control'}),

            'time_additional_dark': forms.TextInput(attrs={'class': 'form-control'}),
            'time_additional_grey': forms.TextInput(attrs={'class': 'form-control'}),
            'time_additional_bright': forms.TextInput(attrs={'class': 'form-control'}),

            'time_grey_moon_quarter': forms.RadioSelect(),

            'dates_preferred': forms.TextInput(
                attrs={'placeholder': 'e.g. Jan, Feb', 'class': 'form-control'}),
            'dates_impossible': forms.TextInput(
                attrs={'placeholder': 'e.g. Jan, Feb', 'class': 'form-control'}),
            'dates_impossible_justification': forms.Textarea(attrs={'class': 'form-control'}),

            'time_constraints_other': forms.Textarea(attrs={'class': 'form-control'}),
            'is_support_required': forms.CheckboxInput(),
            'required_support': forms.Textarea(attrs={'class': 'form-control'}),
            'scientific_justification': forms.FileInput(attrs={'class': 'form-control'}),
        }
        help_texts = {
            'abstract': 'A short summary of the proposed observations (less than 250 words).',
            'abstract_public': 'A short (< 100 words) description of your research written for the general public. This will be used for public relations during your observing period.',
            'scientific_justification': "The scientific and technical justification needs to be supplied as a PDF and shouldn't exceed 2 pages, in a font no smaller than 11pt. Figures, references and object lists can take additional pages if necessary. The technical case should contain information such as the required spatial, spectral and/or temporal resolution, estimates of signal-to-noise ratios, and justification for the number of targets. This description should support the values provided above.",
            'snr': 'Signal-to-noise ratio required to meet your science goals.',
            'time_grey_moon_quarter': "Select the preferred quarter of the Moon for your grey time. Please choose 'I don't care' if you are requesting no grey time.",
            'dates_preferred': 'The preferred dates for your observations (e.g. Jan, Feb).',
            'dates_impossible': 'Dates when your observations are impossible (e.g. Jan, Feb).',
            'dates_impossible_justification': 'Justification for impossible dates (e.g. wrong RAs).',
            'time_constraints_other': 'Please give any other time constraints (e.g. Moon phase/position, specific dates of meetings, other observing time etc.) you wish to avoid clashes with.',
            'required_support': 'A support astronomer is usually only available for the first night of a run. Supervisors should accompany inexperienced students.'
        }

    ApplicantFormset = inlineformset_factory(
        SAAOApplication, Applicant, form=ApplicantForm, formset=BaseApplicantFormset,
        exclude=['application'], extra=1)

    TargetFormset = inlineformset_factory(
        SAAOApplication, Target, formset=RequiredFormset, exclude=['application'],
        extra=1, widgets={
            'name': forms.TextInput(attrs={'class': 'form-control',
                'placeholder': 'Name'}),
            'right_ascension': forms.TextInput(attrs={'class': 'form-control',
                'placeholder': 'Right Ascension'}),
            'declination': forms.TextInput(attrs={'class': 'form-control',
                'placeholder': 'Declination'}),
            'vmagnitude': forms.TextInput(attrs={'class': 'form-control',
                'placeholder': 'V Magnitude'}),
            'comment': forms.TextInput(attrs={'class': 'form-control',
                'placeholder': 'Comments'}),
    })

    OtherApplicationFormset = inlineformset_factory(
        SAAOApplication, OtherApplication, exclude=['application'], extra=1,
        widgets={
            'telescope': forms.TextInput(attrs={'class': 'form-control',
                'placeholder': 'Telescope'}),
            'title': forms.TextInput(attrs={'class': 'form-control',
                'placeholder': 'Title'}),
    })

    PreviousApplicationFormset = inlineformset_factory(
        SAAOApplication, PreviousApplication, exclude=['application'], extra=1,
        widgets={
            'reference': forms.TextInput(attrs={'class': 'form-control',
                'placeholder': 'Reference Number'}),
            'time_awarded': forms.TextInput(attrs={'class': 'form-control',
                'placeholder': 'Amount of Time Awarded'}),
            'useful_percentage': forms.TextInput(attrs={'class': 'form-control',
                'placeholder': 'Useful Percentage'}),
            'comments': forms.TextInput(attrs={'class': 'form-control',
                'placeholder': 'Comments'}),
    })

    def __init__(self, data=None, *args, **kwargs):
        super(ApplicationForm, self).__init__(data, *args, **kwargs)
        application = kwargs.get('instance')

        self.formsets = {
            'applicants': self.ApplicantFormset(data, instance=application),
            'targets': self.TargetFormset(data, instance=application),
            'other_applications': self.OtherApplicationFormset(
                data, instance=application),
            'previous_applications': self.PreviousApplicationFormset(
                data, instance=application),
        }

        # Model forms take hints from the model about which fields are required
        # and since most fields in the Application model are nullable and not
        # required so that we can save drafts, we need to manually mark the
        # required fields.
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        self.fields['email'].required = True

        self.fields['street'].required = True
        self.fields['city'].required = True
        self.fields['postal_code'].required = True
        self.fields['country'].required = True
        self.fields['phone'].required = True

        self.fields['telescope'].required = True
        self.fields['instrument'].required = True
        self.fields['gratings_filters'].required = True

        self.fields['magnitude_range_min'].required = True
        self.fields['magnitude_range_max'].required = True
        self.fields['magnitude_range_filter'].required = True

        self.fields['snr'].required = True

        self.fields['time_dark'].required = True
        self.fields['time_grey'].required = True
        self.fields['time_bright'].required = True

        self.fields['time_min_dark'].required = True
        self.fields['time_min_grey'].required = True
        self.fields['time_min_bright'].required = True

        self.fields['time_grey_moon_quarter'].required = True

        self.fields['abstract'].required = True
        self.fields['abstract_public'].required = True
        self.fields['scientific_justification'].required = True

    def clean(self):
        cleaned_data = super(ApplicationForm, self).clean()
        dates_impossible = cleaned_data.get('dates_impossible')
        dates_impossible_justification = cleaned_data.get('dates_impossible_justification')

        # We need a justification for impossible dates if the user has entered any.
        if dates_impossible and not dates_impossible_justification:
            self.add_error('dates_impossible_justification',
                'This field is required.')


class DraftApplicationForm(ApplicationForm):
    # Redefine formsets without RequiredFormset.
    ApplicantFormset = inlineformset_factory(
        SAAOApplication, Applicant, form=ApplicantForm, exclude=['application'],
        min_num=0, extra=0)

    TargetFormset = inlineformset_factory(
        Application, Target, exclude=['application'], extra=1, widgets={
            'name': forms.TextInput(attrs={'class': 'form-control',
                'placeholder': 'Name'}),
            'right_ascension': forms.TextInput(attrs={'class': 'form-control',
                'placeholder': 'Right Ascension'}),
            'declination': forms.TextInput(attrs={'class': 'form-control',
                'placeholder': 'Declination'}),
            'vmagnitude': forms.TextInput(attrs={'class': 'form-control',
                'placeholder': 'V Magnitude'}),
            'comment': forms.TextInput(attrs={'class': 'form-control',
                'placeholder': 'Comment'}),
    })

    def __init__(self, *args, **kwargs):
        super(DraftApplicationForm, self).__init__(*args, **kwargs)

        # Everything except for the title is optional when saving a draft
        # application.
        for field in self.fields:
            if field != 'title':
                self.fields[field].required = False

    # Intentionally do nothing here so that the justification for impossible
    # dates isn't validated.
    def clean(self):
        pass
