from __future__ import unicode_literals

import os
import datetime

from io import BytesIO
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib import colors
from reportlab.lib.units import cm
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.platypus import (
    SimpleDocTemplate, Paragraph, Spacer, Table, TableStyle)
from PyPDF2 import PdfFileReader, PdfFileWriter
from django import forms
from django.conf import settings
from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.forms.models import inlineformset_factory
from django.template.loader import render_to_string
from django_fsm import transition
from portal.models import Application, Applicant
from portal.applications.forms import (
    RequiredFormset, ApplicantForm, BaseApplicationForm)


pdfmetrics.registerFont(TTFont('NotoSans',
    os.path.join(settings.STATIC_ROOT, 'fonts', 'NotoSans-unhinted',
        'NotoSans-Regular.ttf')))
pdfmetrics.registerFont(TTFont('NotoSansBold',
    os.path.join(settings.STATIC_ROOT, 'fonts', 'NotoSans-unhinted',
        'NotoSans-Bold.ttf')))
pdfmetrics.registerFont(TTFont('NotoSansItalic',
    os.path.join(settings.STATIC_ROOT, 'fonts', 'NotoSans-unhinted',
        'NotoSans-Italic.ttf')))
pdfmetrics.registerFont(TTFont('NotoSansBoldItalic',
    os.path.join(settings.STATIC_ROOT, 'fonts', 'NotoSans-unhinted',
        'NotoSans-BoldItalic.ttf')))


class LCOGTApplication(Application):
    class Meta:
        proxy = True

    def as_pdf(self):
        path = os.path.join(
            settings.MEDIA_ROOT, 'applications', '{}.pdf'.format(
                self.reference)).encode('utf-8')

        if os.path.exists(path) and self.state != 'draft':
            # A small workaround for when we've lost the original scientific
            # justification document, but we have stored the complete
            # application previously.
            with open(path) as f:
                buffer = BytesIO(f.read())
        else:
            styles = getSampleStyleSheet()

            styles['BodyText'].fontName = 'NotoSans'

            h1 = styles['Heading1']
            h1.alignment = 1
            h1.fontSize = 14

            h2 = styles['Heading2']
            h2.fontSize = 11
            h2.leading = 20

            h3 = styles['Heading3']
            h3.fontName = 'Helvetica'
            h3.fontSize = 10

            paragraph = styles['Normal']
            paragraph.fontSize = 9
            paragraph.fontName = 'NotoSans'

            table_style = TableStyle([
                ('LEFTPADDING', (0,0), (-1,-1), 0),
                ('RIGHTLEFTPADDING', (0,0), (-1,-1), 0),
                ('FONTSIZE', (0,0), (-1,-1), 9),
                ('FONTNAME', (0,0), (-1,-1), 'NotoSans'),
                ('LINEABOVE', (0,0), (-1,0), 2, colors.black),
                ('LINEABOVE', (0,1), (-1,-1), 0.25, colors.grey),
                ('VALIGN', (0,0), (-1,-1), 'TOP')])

            story = []

            story.append(Paragraph('Telescope Time Applications', h1))

            title = h1
            title.alignment = 1
            title.fontSize = 13
            title.leading = 12
            title.spaceBefore = 20
            title.spaceAfter = 20

            story.append(Paragraph(self.title, title))

            applicants = self.applicants.all()
            if applicants:
                story.append(Paragraph('Applicants', h2))
                data = [
                    [Paragraph('<b>Name</b>', h3),
                        Paragraph('<b>Institution</b>', h3)]
                ]
                for applicant in applicants:
                    data.append([
                        applicant,
                        applicant.institution
                    ])
                story.append(Table(data, style=table_style))

            story.append(Paragraph('Principal Contact', h2))
            data = [
                [Paragraph('<b>Name</b>', h3),
                    Paragraph('<b>Phone</b>', h3),
                    Paragraph('<b>Email</b>', h3)],
                ['{} {}'.format(self.first_name, self.last_name),
                    self.phone,
                    self.email],
                [Paragraph('<b>Postal Address</b', h3)],
                [Paragraph(self.postal_address, paragraph)],
                [Paragraph('<b>City</b>', h3),
                    Paragraph('<b>Zip / Postal Code</b>', h3),
                    Paragraph('<b>Country</b>', h3)],
                [self.city, self.postal_code,
                        self.get_country_display()]
            ]
            custom_table_style = table_style.getCommands()+[('SPAN',(0,3),(2,3),)]
            table = Table(data, style=custom_table_style)
            story.append(table)

            story.append(Paragraph('Abstract', h2))
            data = [
                [Paragraph(self.abstract.replace('\n\n', '<br/><br/>'), paragraph)],
            ]
            story.append(Table(data, style=table_style))
            story.append(Spacer(1, 0.5*cm))

            # Telescope, Instrument
            data = [
                [Paragraph('<b>Telescope</b>', h3), Paragraph('<b>Instrument</b>', h3),],
                ['None' if not self.telescope else self.telescope.name,
                    'None' if not self.instrument else self.instrument.name]
            ]
            story.append(Table(data, style=table_style))

            buffer = BytesIO()

            doc = SimpleDocTemplate(
                buffer, leftMargin=25, rightMargin=25, topMargin=25, bottomMargin=25)
            doc.title = self.title
            doc.author = str(self.created_by)
            doc.build(story)

            output = PdfFileWriter()
            docs = [buffer]
            if self.scientific_justification:
                docs.append(open(self.scientific_justification.file.name, 'rb'))
            for doc in docs:
                reader = PdfFileReader(doc)
                for page in xrange(reader.numPages):
                    output.addPage(reader.getPage(page))

            buffer = BytesIO()
            output.write(buffer)

            # Save the generated document for later.
            with open(path, 'wb+') as f:
                f.write(buffer.getvalue())

        return buffer

    @transition(field='state', source='draft', target='submitted', permission='portal.submit_application')
    def submit(self):
        # Only set the reference if it hasn't been set already. This keeps the
        # reference the same for resubmitted applications.
        if not self.reference:
            self.reference = '-'.join([self.last_name.replace(' ', '-'),
                datetime.datetime.utcnow().strftime('%Y-%m'),
                self.telescope.name.replace(' ', '-'), str(self.id)])

        attachments = [('{}.pdf'.format(
            self.reference), self.as_pdf().getvalue(), 'application/pdf')]
        message = render_to_string('applications/lcogt/emails/submitted.txt', {
            'application': self
        })
        email = EmailMessage(
            '[{}] Your Telescope Time Application'.format(self.facility),
            message, settings.DEFAULT_FROM_EMAIL, [self.email],
            reply_to=['observer@saao.ac.za'], attachments=attachments)
        # Include the submitter if they are not the same as the principal contact.
        if self.email != self.created_by.email:
            email.cc = [self.created_by.email]
        email.send()

        text = render_to_string('applications/lcogt/emails/submitted_notification.txt', {
            'application': self,
        })
        html = render_to_string('applications/lcogt/emails/submitted_notification.html', {
            'application': self,
        })

        email = EmailMultiAlternatives(
            '[{}] New Telescope Time Application'.format(self.facility),
            text, settings.DEFAULT_FROM_EMAIL, [email for name, email in settings.MANAGERS],
            reply_to=[self.email], attachments=attachments)
        email.attach_alternative(html, 'text/html')
        email.send()


class BaseApplicantFormset(RequiredFormset):
    def clean(self):
        if any(self.errors):
            return

        emails = []
        for form in self.forms:
            if not form.cleaned_data or form.cleaned_data['DELETE']:
                continue

            email = form.cleaned_data['email']
            if email in emails:
                raise forms.ValidationError(
                    'Each applicant must have a unique email address.')
            emails.append(email)


class ApplicationForm(BaseApplicationForm):
    class Meta:
        model = LCOGTApplication
        exclude = [
            'reference', 'created_by', 'state', 'call', 'time_additional_dark',
            'time_additional_grey', 'time_additional_bright',]
        labels = {
            'postal_code': 'Zip / Postal Code',
        }
        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'form-control'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control'}),
            'email': forms.EmailInput(attrs={'class': 'form-control'}),
            'title': forms.TextInput(attrs={'class': 'form-control'}),
            'abstract': forms.Textarea(attrs={'class': 'form-control'}),
            'street': forms.TextInput(attrs={'class': 'form-control'}),
            'street2': forms.TextInput(attrs={'class': 'form-control'}),
            'city': forms.TextInput(attrs={'class': 'form-control'}),
            'postal_code': forms.TextInput(attrs={'class': 'form-control'}),
            'country': forms.Select(attrs={'class': 'form-control'}),
            'phone': forms.TextInput(attrs={'class': 'form-control'}),
        }
        help_texts = {
            'abstract': 'A short summary of the proposed observations (less than 250 words).',
            'scientific_justification': "The scientific justification should be no more than 2 pages of A4. It is essential that on-going multi-semester programmes need to report on progress so far. The technical case should include observing strategy and total amount of time requested. Account for every hour requested. State minimum amount of useful time. The proposal will also be judged on the appropriate use of the strengths of the LCO.",
        }

    ApplicantFormset = inlineformset_factory(
        LCOGTApplication, Applicant, form=ApplicantForm, formset=BaseApplicantFormset,
        exclude=['application'], extra=1)

    def __init__(self, data=None, *args, **kwargs):
        super(ApplicationForm, self).__init__(data, *args, **kwargs)
        application = kwargs.get('instance')

        self.formsets = {
            'applicants': self.ApplicantFormset(data, instance=application),
        }

        # Manually mark required fields.
        self.fields['first_name'].required = True
        self.fields['last_name'].required = True
        self.fields['email'].required = True

        self.fields['street'].required = True
        self.fields['city'].required = True
        self.fields['postal_code'].required = True
        self.fields['country'].required = True
        self.fields['phone'].required = True

        self.fields['telescope'].required = True
        self.fields['instrument'].required = True

        self.fields['abstract'].required = True
        self.fields['scientific_justification'].required = True


class DraftApplicationForm(ApplicationForm):
    # Redefine formsets without RequiredFormset.
    ApplicantFormset = inlineformset_factory(
        LCOGTApplication, Applicant, form=ApplicantForm, exclude=['application'],
        min_num=0, extra=0)

    def __init__(self, *args, **kwargs):
        super(DraftApplicationForm, self).__init__(*args, **kwargs)

        # Everything except for the title is optional when saving a draft.
        for field in self.fields:
            if field != 'title':
                self.fields[field].required = False
