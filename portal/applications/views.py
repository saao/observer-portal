import json

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse, Http404
from django.shortcuts import render, redirect, get_object_or_404
from django.utils import timezone
from portal.applications import saao, lcogt, kmtnet
from portal.applications.saao import SAAOApplication
from portal.applications.lcogt import LCOGTApplication
from portal.applications.kmtnet import KMTNETApplication
from portal.models import Instrument, Application, Facility, Call


@login_required
def applications(request):
    if not request.user.has_perm('portal.add_application'):
        raise PermissionDenied

    applications = Application.objects.filter(created_by=request.user)

    # Filter applications by state or facility.
    if 'state' in request.GET:
        applications = applications.filter(state=request.GET['state'])
    if 'facility' in request.GET:
        applications = applications.filter(facility__pk=request.GET['facility'])

    return render(request, 'applications/index.html', {
        'applications': applications,
        'facilities': Facility.objects.all(),
    })


@login_required
def copy(request, facility):
    """Choose an application to copy."""

    if not request.user.has_perm('portal.add_application'):
        raise PermissionDenied

    facility = get_object_or_404(Facility, name=facility)
    applications = Application.objects.filter(
        created_by=request.user, facility=facility).exclude(state='draft')

    return render(request, 'applications/copy.html', {
        'applications': applications})


@login_required
def create(request, facility=None, copy_from_pk=None):
    if not request.user.has_perm('portal.add_application'):
        raise PermissionDenied

    # The user needs to select a facility first.
    if facility is None:
        return render(request, 'applications/create.html')

    facility = get_object_or_404(Facility, name=facility)

    # Set the form and template based on the selected facility.
    if facility.name == 'SAAO':
        ApplicationForm = saao.ApplicationForm
        if request.method == 'POST' and 'draft' in request.POST:
            ApplicationForm = saao.DraftApplicationForm
        template_name = 'applications/saao/create.html'
    elif facility.name == 'LCO':
        ApplicationForm = lcogt.ApplicationForm
        if request.method == 'POST' and 'draft' in request.POST:
            ApplicationForm = lcogt.DraftApplicationForm
        template_name = 'applications/lcogt/create.html'
    elif facility.name == 'KMTNET':
        ApplicationForm = kmtnet.ApplicationForm
        if request.method == 'POST' and 'draft' in request.POST:
            ApplicationForm = kmtnet.DraftApplicationForm
        template_name = 'applications/kmtnet/create.html'
    else:
        raise Http404

    telescopes = facility.telescopes.filter(is_available=True)
    instruments = Instrument.objects.filter(
        is_available=True, telescope__in=telescopes).distinct()

    if request.method == 'POST':
        form = ApplicationForm(request.POST, request.FILES)
        form.fields['telescope'].queryset = telescopes
        form.fields['instrument'].queryset = instruments

        if form.is_valid():
            application = form.save(created_by=request.user)
            if 'submit' in request.POST:
                return redirect('applications:submit', pk=application.pk)
            messages.success(request, 'Your application has been saved.')
            return redirect('applications:index')

        # If the form isn't valid and the user has selected a telescope, filter
        # down the available list of instruments based on that.
        if form.cleaned_data.get('telescope'):
            form.fields['instrument'].queryset = form.cleaned_data['telescope'].instruments.filter(is_available=True)
        else:
            form.fields['instrument'].queryset = Instrument.objects.none
    else:
        copy = None
        if copy_from_pk is not None:
            copy = Application.objects.filter(pk=copy_from_pk,
                created_by=request.user, facility=facility).first()

        form = ApplicationForm(instance=copy, initial={
            'facility': facility,
            'first_name': request.user.first_name,
            'last_name': request.user.last_name,
            'email': request.user.email,
            'street': request.user.profile.street,
            'street2': request.user.profile.street2,
            'city': request.user.profile.city,
            'postal_code': request.user.profile.postal_code,
            'country': request.user.profile.country,
            'phone': request.user.profile.phone,
        })

        form.fields['telescope'].queryset = telescopes

        # If there's only one telescope, filter instruments to those available
        # for the telescope.
        if len(telescopes) == 1:
            form.fields['instrument'].queryset = Instrument.objects.filter(
                telescope__in=telescopes, is_available=True)

    _telescopes = {}
    for t in telescopes:
        _telescopes[t.id] = {
            'name': t.name,
            'instruments': [i.id for i in t.instruments.filter(
                is_available=True)]
        }

    return render(request, template_name, {
        'form': form,
        'facility': facility,
        'telescopes': json.dumps(_telescopes),
        'instruments': json.dumps({i.id: i.name for i in instruments}),
    })


@login_required
def update(request, pk):
    application = get_object_or_404(Application, pk=pk,
        created_by=request.user)

    # Set the form and template based on the facility associated with this
    # application.
    if application.facility.name == 'SAAO':
        ApplicationForm = saao.ApplicationForm
        if request.method == 'POST' and 'draft' in request.POST:
            ApplicationForm = saao.DraftApplicationForm

        if application.state == 'draft':
            template_name = 'applications/saao/update.html'
        else:
            template_name = 'applications/saao/view.html'

    elif application.facility.name == 'LCO':
        ApplicationForm = lcogt.ApplicationForm
        if request.method == 'POST' and 'draft' in request.POST:
            ApplicationForm = lcogt.DraftApplicationForm

        if application.state == 'draft':
            template_name = 'applications/lcogt/update.html'
        else:
            template_name = 'applications/lcogt/view.html'

    elif application.facility.name == 'KMTNET':
        ApplicationForm = kmtnet.ApplicationForm
        if request.method == 'POST' and 'draft' in request.POST:
            ApplicationForm = kmtnet.DraftApplicationForm

        if application.state == 'draft':
            template_name = 'applications/kmtnet/update.html'
        else:
            template_name = 'applications/kmtnet/view.html'

    telescopes = application.facility.telescopes.filter(is_available=True)
    instruments = Instrument.objects.filter(
        is_available=True, telescope__in=telescopes).distinct()

    if request.method == 'POST':
        if 'delete' in request.POST:
            application.delete()
            messages.success(request, 'Your application was deleted.')
            return redirect('applications:index')

        form = ApplicationForm(request.POST, request.FILES, instance=application)
        form.fields['telescope'].queryset = telescopes
        form.fields['instrument'].queryset = instruments

        if form.is_valid():
            form.save()
            if 'submit' in request.POST:
                return redirect('applications:submit', pk=application.pk)
            messages.success(request, 'Your application has been saved.')
            return redirect('applications:index')

        # If the form isn't valid and the user has selected a telescope, filter
        # down the available list of instruments based on that.
        if 'telescope' in form.cleaned_data:
            form.fields['instrument'].queryset = form.cleaned_data['telescope'].instruments.filter(is_available=True)
        else:
            form.fields['instrument'].queryset = Instrument.objects.none
    else:
        form = ApplicationForm(instance=application)

        form.fields['telescope'].queryset = telescopes

        # If there's only one telescope, filter instruments to those available
        # for the telescope.
        if len(telescopes) == 1:
            form.fields['instrument'].queryset = Instrument.objects.filter(
                is_available=True, telescope__in=telescopes)

    _telescopes = {}
    for t in telescopes:
        _telescopes[t.id] = {
            'name': t.name,
            'instruments': [i.id for i in t.instruments.filter(
                is_available=True)]
        }

    return render(request, template_name, {
        'form': form,
        'application': application,
        'telescopes': json.dumps(_telescopes),
        'instruments': json.dumps({i.id: i.name for i in instruments}),
    })


@login_required
def submit(request, pk):
    application = get_object_or_404(Application, pk=pk,
        created_by=request.user, state='draft')

    # We'll associate the application with the first available call for the
    # facility.
    call = Call.objects.filter(
        deadline__gt=timezone.now(), facility=application.facility).first()

    if request.method == 'POST':
        # We don't want to change the call if the application is already
        # assigned to one and is being resubmitted.
        if not application.call:
            application.call = call
        application.submit()
        application.save()
        messages.success(request, 'Thank you for submitting your proposal! '
            'You should receive a confirmation email shortly.')
        return redirect('applications:update', pk=application.pk)

    # Set template based on the facility associated with this application.
    if application.facility.name == 'SAAO':
        template_name = 'applications/saao/submit.html'
    if application.facility.name == 'LCO':
        template_name = 'applications/lcogt/submit.html'
    if application.facility.name == 'KMTNET':
        template_name = 'applications/kmtnet/submit.html'

    return render(request, template_name, {
        'application': application,
        'call': call,
    })


@login_required
def download(request, reference):
    application = get_object_or_404(Application, reference=reference,
        created_by=request.user)

    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="{}.pdf"'.format(
        application)

    buffer = application.as_pdf()
    pdf = buffer.getvalue()
    response.write(pdf)

    return response
