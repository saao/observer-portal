from django.conf.urls import include, url
from portal.applications import views


urlpatterns = [
    url(r'^$', views.applications, name='index'),
    url(r'^(?P<reference>[^/]+).pdf$', views.download, name='download'),

    url(r'^new$', views.create, name='new'),

    url(r'^(?P<facility>\w+)/', include([
        url(r'^new$', views.create, name='create'),
        url(r'^new/(?P<copy_from_pk>\d+)$', views.create, name='create'),
        url(r'^copy$', views.copy, name='copy'),
    ])),

    url(r'^(?P<pk>\d+)/', include([
        url(r'^$', views.update, name='update'),
        url(r'^submit$', views.submit, name='submit'),
    ])),
]
