from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site


def version(request):
    return {'VERSION': settings.VERSION}


def site(request):
    """Makes the full site URL available in the template context."""

    current_site = get_current_site(request)

    return {
        'site': current_site,
        'SITE_URL': lambda: '{}://{}'.format(
            'https' if request.is_secure() else 'http', current_site.domain)
    }
