(function($) {
    $('#fake_license_button').click(function() {
        $('#id_drivers_license').click();
    });

    $('#fake_passport_button').click(function() {
        $('#id_passport').click();
    });

    $('#id_drivers_license').change(function() {
        var file = $('#id_drivers_license')[0].files[0];
        var url = window.URL.createObjectURL(file);
        console.log(file.name);
        $('#drivers_license_name').html('<span class="glyphicon glyphicon-paperclip"></span><a href="' + url + '"target="_blank">&nbsp;' + file.name + '</a>');
    });

    $('#id_passport').change(function() {
        var file = $('#id_passport')[0].files[0];
        var url = window.URL.createObjectURL(file);
        console.log(file.name);
        $('#passport_name').html('<span class="glyphicon glyphicon-paperclip"></span><a href="' + url + '"target="_blank">&nbsp;' + file.name + '</a>');
    });
})(jQuery);
