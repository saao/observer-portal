(function($) {

    $('.add-applicant').click(function() {
        var $totalForms = $('#id_applicants-TOTAL_FORMS'),
            total = parseInt($totalForms.val(), 10),
            form = $('#empty_applicant_form').html().replace(/__prefix__/g, total);

        $('#applicants').append(form);
        $totalForms.val(total + 1);
    });
    $(document).on('click', '.delete-applicant', function(e) {
        var $totalForms = $('#id_applicants-TOTAL_FORMS'),
            total = parseInt($totalForms.val(), 10);
        $(e.target).closest('.row').remove();
        $totalForms.val(total - 1);
    });

    $('.add-target').click(function() {
        var $totalForms = $('#id_targets-TOTAL_FORMS'),
            total = parseInt($totalForms.val(), 10),
            form = $('#empty-target-form').html().replace(/__prefix__/g, total);
        $('#targets').append(form);
        $totalForms.val(total + 1);
    });
    $(document).on('click', '.delete-target', function(e) {
        var $totalForms = $('#id_targets-TOTAL_FORMS'),
            total = parseInt($totalForms.val(), 10);
        $(e.target).closest('.row').remove();
        $totalForms.val(total - 1);
    });

    $('.add-other-application').click(function() {
        var $totalForms = $('#id_other_applications-TOTAL_FORMS'),
            total = parseInt($totalForms.val(), 10),
            form = $('#empty-other-application-form').html().replace(/__prefix__/g, total);
        $('#other-applications').append(form);
        $totalForms.val(total + 1);
    });
    $(document).on('click', '.delete-other-application', function(e) {
        var $totalForms = $('#id_other_applications-TOTAL_FORMS'),
            total = parseInt($totalForms.val(), 10);
        $(e.target).closest('.row').remove();
        $totalForms.val(total - 1);
    });

    $('.add-previous-application').click(function() {
        var $totalForms = $('#id_previous_applications-TOTAL_FORMS'),
            total = parseInt($totalForms.val(), 10),
            form = $('#empty-previous-application-form').html().replace(/__prefix__/g, total);
        $('#previous-applications').append(form);
        $('#id_previous_applications-TOTAL_FORMS').val(total + 1);
    });
    $(document).on('click', '.delete-previous-application', function(e) {
        var $totalForms = $('#id_previous_applications-TOTAL_FORMS'),
            total = parseInt($totalForms.val(), 10);
        $(e.target).closest('.row').remove();
        $totalForms.val(total - 1);
    });

    $('#fake_upload_button').click(function() {
        $('#id_scientific_justification').click();
    });

    $('#id_scientific_justification').change(function() {
        var file = $('#id_scientific_justification')[0].files[0];
        var url = window.URL.createObjectURL(file);
        $('#file_name').html('<span class="glyphicon glyphicon-paperclip"></span><a href="' + url +'" target="_blank">&nbsp;' + file.name + '</a>');
    });

    // Update the options in the instrument field when the selected telescope
    // changes.
    $('#id_telescope').change(function(e) {
        var $instrument = $('#id_instrument'),
            telescope = telescopes[e.target.value];
            options = [];

        if (typeof telescope === 'undefined') {
            options.push('<option value="" selected="selected">Please choose a telescope</option>');
        } else {
            options.push('<option></option>');
            // If the telescope only has one instrument, automatically select
            // that for the user.
            if (telescope.instruments.length == 1) {
                options.push('<option value="' + telescope.instruments[0] + '" selected="selected">' + instruments[telescope.instruments[0]] + '</option>');
            } else {
                telescope.instruments.forEach(function(id) {
                    options.push('<option value="' + id + '">' + instruments[id] + '</option>');
                });
            }
        }
        $instrument.html(options);
    });
})(jQuery);
