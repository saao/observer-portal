import json

from datetime import datetime, timedelta
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.utils import timezone
from oauth2_provider.views.generic import ProtectedResourceView
from oauth2_provider.views import AuthorizationView
from .forms import UserForm, UserProfileForm
from .auth.models import User
from .models import Telescope, Instrument, Call, Report


class AuthView(AuthorizationView):
    def redirect(self, redirect_to, application):
        # Delete the session before redirecting.
        self.request.session.delete()

        return super(AuthView, self).redirect(redirect_to, application)


class ApiEndpoint(ProtectedResourceView):
    def get(self, request, *args, **kwargs):
        return HttpResponse(json.dumps({
            'id': request.resource_owner.pk,
            'username': request.resource_owner.username,
            'email': request.resource_owner.email,
        }))


def dashboard(request):
    calls = Call.objects.filter(deadline__gt=timezone.now())
    telescopes = Telescope.objects.all()
    instruments = Instrument.objects.all()
    reports = Report.objects.all()
    last_report_date = None
    if reports:
        last_report_date = reports.order_by('-created_at')[0].created_at
    return render(request, 'dashboard.html', {
        'calls': calls,
        'telescopes': telescopes,
        'instruments': instruments,
        'reports': reports,
        'now': timezone.now(),
        'last_report_date': last_report_date,
    })


def telescope(request, pk):
    """Displays a telescope status page."""

    telescope = get_object_or_404(Telescope, pk=pk)

    context = {
        'telescope': telescope,
        'telescopes': Telescope.objects.all(),
        'instruments': Instrument.objects.all(),
    }

    if hasattr(telescope, 'category'):
        try:
            start_date = datetime.strptime(request.GET.get('start'), '%Y-%m-%d')
            end_date = datetime.strptime(request.GET.get('end'), '%Y-%m-%d')
        except (TypeError, ValueError):
            end_date = datetime.utcnow()
            start_date = end_date - timedelta(days=30)

        context.update({
            'start_date': start_date,
            'end_date': end_date,
            'logs': telescope.category.logs.filter(
                created_at__gte=start_date.replace(hour=0, minute=0, second=0),
                created_at__lte=end_date.replace(hour=23, minute=59, second=59)),
        })

    return render(request, 'telescope.html', context)


def instrument(request, pk):
    """Displays an insrument status page."""

    instrument = get_object_or_404(Instrument, pk=pk)

    return render(request, 'instrument.html', {
        'instrument': instrument,
        'telescopes': Telescope.objects.all(),
        'instruments': Instrument.objects.all(),
    })


def service_observing(request):
    return render(request, 'service_observing.html')


@login_required
def profile(request):
    user = User.objects.get(pk=request.user.id)
    if request.method == 'POST':
        user_form = UserForm(request.POST, instance=user)
        user_profile_form = UserProfileForm(
            request.POST, request.FILES, instance=user.profile)
        if user_form.is_valid() and user_profile_form.is_valid():
            user_form.save()
            user_profile_form.save()
            messages.success(request, 'Your profile was updated successfully.')
            return redirect('dashboard')
    else:
        user_form = UserForm(instance=user)
        user_profile_form = UserProfileForm(instance=user.profile)
    return render(request, 'profile.html', {
            'user_form': user_form,
            'user_profile_form': user_profile_form})
