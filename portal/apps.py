from django.apps import AppConfig
from django.db.models.signals import post_save
from django.contrib.auth import get_user_model


def create_user_profile(sender, instance, created, **kwargs):
    """Creates a user profile everytime a user is created."""
    from .models import UserProfile
    if created:
        UserProfile.objects.get_or_create(user=instance)


def add_to_default_group(sender, instance, created, **kwargs):
    from django.contrib.auth.models import Group
    if created:
        group = Group.objects.get(name='Observers')
        instance.groups.add(group)


class PortalConfig(AppConfig):
    name = 'portal'
    label = 'portal'

    def ready(self):
        User = get_user_model()
        post_save.connect(create_user_profile, sender=User)
        post_save.connect(add_to_default_group, sender=User)
