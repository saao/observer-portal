from django.conf.urls import include, url
from . import views


urlpatterns = [
    url(r'^applications/', include([
        url(r'^$', views.applications.index, name='index'),

        url(r'^(?P<pk>\d+)/', include([
            url(r'^$', views.applications.edit, name='edit'),
            url(r'^approve$', views.applications.approve, name='approve'),
            url(r'^reject$', views.applications.reject, name='reject'),
        ])),

        url(r'^(?P<reference>[^/]+).pdf$',
            views.applications.download, name='download'),
    ], namespace='applications')),

    url(r'^applications.csv', views.applications.rota, name='rota'),

    url(r'^facilities/', include([
        url(r'^$', views.facilities.index, name='index'),
        url(r'^new$', views.facilities.add, name='add'),
        url(r'^(?P<pk>\d+)/$', views.facilities.edit, name='edit'),
    ], namespace='facilities')),

    url(r'^call/new$', views.call_for_proposals, name='call_for_proposals'),

    url(r'calls/', include([
        url(r'^$', views.calls.index, name='index'),
        url(r'^new$', views.calls.add, name='add'),
        url(r'^(?P<pk>\d+)/', views.calls.edit, name='edit'),
    ], namespace='calls')),

    url(r'^telescopes/', include([
        url(r'^$', views.telescopes.index, name='index'),
        url(r'^new$', views.telescopes.add, name='add'),
        url(r'^(?P<pk>\d+)/$', views.telescopes.edit, name='edit'),
    ], namespace='telescopes')),

    url(r'^instruments/', include([
        url(r'^$', views.instruments.index, name='index'),
        url(r'^new$', views.instruments.add, name='add'),
        url(r'^(?P<pk>\d+)/$', views.instruments.edit, name='edit'),
    ], namespace='instruments')),

    url(r'^users/', include([
        url(r'^$', views.users.index, name='index'),
        url(r'^new$', views.users.add, name='add'),
        url(r'^(?P<pk>\d+)/$', views.users.edit, name='edit'),
    ], namespace='users')),

    url(r'^groups/', include([
        url(r'^$', views.groups.index, name='index'),
        url(r'^new$', views.groups.add, name='add'),
        url(r'^(?P<pk>\d+)/$', views.groups.edit, name='edit'),
    ], namespace='groups')),

    url(r'^sites/', include([
        url(r'^$', views.sites.index, name='index'),
        url(r'^new$', views.sites.add, name='add'),
        url(r'^(?P<pk>\d+)/$', views.sites.edit, name='edit'),
    ], namespace='sites')),

    # Only light pollution reports for now, but that might change later.
    url(r'^reports/', include([
        url(r'^new$', views.reports.create, name='create'),
    ], namespace='reports')),

    url(r'^calendar/$', views.calendar, name='calendar'),
]
