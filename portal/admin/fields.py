from django import forms
from django.core.validators import validate_email


class MultiEmailField(forms.Field):
    def to_python(self, value):
        """Normalize data to a list of strings."""

        if not value:
            return []
        return [e.strip() for e in value.split(',')]

    def validate(self, value):
        """Check if value consists only of valid emails."""

        super(MultiEmailField, self).validate(value)

        for email in value:
            validate_email(email)
