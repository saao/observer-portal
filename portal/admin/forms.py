from django import forms
from django.conf import settings
from django.core.mail import EmailMessage
from portal.models import Call
from portal.admin.fields import MultiEmailField


class EmailForm(forms.Form):
    """A generic form used to compose email messages."""

    to = MultiEmailField(widget=forms.TextInput(attrs={
        'class': 'form-control'}))
    cc = MultiEmailField(widget=forms.TextInput(attrs={
        'class': 'form-control'}), required=False)
    subject = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control no-border no-padding',
        'placeholder': 'Subject',
        'style': 'font-size: 18px; height: initial;',
        'autofocus': True,
    }))
    message = forms.CharField(widget=forms.Textarea(attrs={
        'class': 'form-control',
        'style': 'max-width: 100%',
        'rows': 20,
    }))

    def send(self, from_, bcc=None):
        data = self.cleaned_data
        email = EmailMessage(data['subject'], data['message'],
            settings.DEFAULT_FROM_EMAIL, data['to'], cc=data['cc'],
            bcc=bcc, reply_to=[from_])
        email.send()


class CallForProposalsForm(forms.ModelForm):
    class Meta:
        model = Call
        fields = ('name', 'deadline', 'facility',)
        widgets = {
            'name': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'e.g. Trimester I, 2016 (Jan-Apr)',
            }),
            'deadline': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'YYYY-MM-DD HH:MM',
            }),
            'facility': forms.Select(attrs={'class': 'form-control'}),
        }


class AssignCallForm(forms.Form):
    # ModelChoiceField doesn't support sliced querysets so use a normal
    # ChoiceField instead. See: https://code.djangoproject.com/ticket/23870
    call = forms.ChoiceField(widget=forms.RadioSelect())

    def __init__(self, application, *args, **kwargs):
        super(AssignCallForm, self).__init__(*args, **kwargs)

        # Choices depend on the facility the application is assigned to. Limit
        # to the 5 latest calls.
        self.fields['call'].choices = [(c.pk, c.name) for c in Call.objects.filter(
            facility=application.facility)[:5]]
