from django import forms
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views.generic.edit import CreateView
from portal.admin.decorators import user_is_staff
from portal.models import Report


decorators = [login_required, user_is_staff]


class Form(forms.ModelForm):
    class Meta:
        model = Report
        fields = ('lat', 'lng', 'comment', 'observed_at',)
        labels = {
            'observed_at': 'Observed At',
        }
        widgets = {
            'comment': forms.Textarea(attrs={'class': 'form-control'}),
            'observed_at': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'YYYY-MM-DD HH:MM',
            }),
        }

    def clean_observed_at(self):
        """Converts observed_at from UTC to the default timezone.

        We do this because people will presumably always be logging reports in
        whatever the local time is at settings.TIME_ZONE.
        """

        return timezone.get_default_timezone().localize(
            self.cleaned_data['observed_at'].replace(tzinfo=None))


@method_decorator(decorators, name='dispatch')
class ReportCreateView(CreateView):
    model = Report
    template_name = 'admin/reports/create.html'
    form_class = Form

    def get_initial(self):
        return {
            'observed_at': timezone.localtime(
                timezone.now(), timezone.get_default_timezone()).strftime('%Y-%m-%d %H:%M')
        }

    def form_valid(self, form):
        report = form.save(commit=False)
        report.created_by = self.request.user
        messages.success(self.request,
            'Thank you! Your light pollution report was logged successfully.')
        return super(ReportCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse('dashboard')
create = ReportCreateView.as_view()
