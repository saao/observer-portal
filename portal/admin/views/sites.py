from django import forms
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.sites.models import Site
from django.utils.decorators import method_decorator
from django.forms.models import modelform_factory
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView
from portal.admin.decorators import user_is_staff


decorators = [login_required, user_is_staff]


@method_decorator(decorators, name='dispatch')
class SiteListView(ListView):
    model = Site
    template_name = 'admin/sites/index.html'
    context_object_name = 'sites'
index = SiteListView.as_view()


@method_decorator(decorators, name='dispatch')
class SiteAddView(CreateView):
    model = Site
    template_name = 'admin/sites/add.html'
    fields = ('name', 'domain',)
    widgets = {
        'name': forms.TextInput(attrs={'class': 'form-control'}),
        'domain': forms.TextInput(attrs={'class': 'form-control'}),
    }

    def get_form_class(self):
        return modelform_factory(self.model, fields=self.fields, widgets=self.widgets)

    def form_valid(self, form):
        messages.success(self.request, 'The site was added successfully.')
        return super(SiteAddView, self).form_valid(form)

    def get_success_url(self):
        return reverse('admin:sites:index')
add = SiteAddView.as_view()


@method_decorator(decorators, name='dispatch')
class SiteEditView(UpdateView):
    model = Site
    template_name = 'admin/sites/edit.html'
    fields = ('name', 'domain',)
    widgets = {
        'name': forms.TextInput(attrs={'class': 'form-control'}),
        'domain': forms.TextInput(attrs={'class': 'form-control'}),
    }

    def get_form_class(self):
        return modelform_factory(self.model, fields=self.fields, widgets=self.widgets)

    def form_valid(self, form):
        messages.success(self.request, 'The site was updated successfully.')
        return super(SiteEditView, self).form_valid(form)

    def get_success_url(self):
        return reverse('admin:sites:index')
edit = SiteEditView.as_view()
