import csv

from django.shortcuts import render, redirect, get_object_or_404
from django.core.exceptions import PermissionDenied
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group
from django.template.loader import render_to_string
from django.http import HttpResponse
from portal.models import Application, Call
from portal.applications.saao import SAAOApplication
from portal.applications.lcogt import LCOGTApplication
from portal.applications.kmtnet import KMTNETApplication
from portal.admin.forms import EmailForm, AssignCallForm


@login_required
def index(request):
    if not (request.user.is_staff and
            request.user.has_perm('portal.view_application')):
        raise PermissionDenied

    applications = Application.objects.exclude(state='draft')
    if 'state' in request.GET:
        applications = applications.filter(state=request.GET['state'])
    if 'call' in request.GET:
        applications = applications.filter(call=request.GET['call'])

    return render(request, 'admin/applications/index.html', {
        'applications': applications,
        'calls': Call.objects.all(),
    })


@login_required
def edit(request, pk):
    if not (request.user.is_staff and
            request.user.has_perm('portal.view_application')):
        raise PermissionDenied

    application = get_object_or_404(Application, pk=pk)

    if request.method == 'POST':
        if not request.user.has_perm('portal.change_application'):
            raise PermissionDenied

        if '_assign' in request.POST:
            form = AssignCallForm(application, request.POST)

            if form.is_valid():
                application.call_id = form.cleaned_data['call']
                application.save()

                messages.success(request,
                    'The application was successfully assigned to a call.')

        elif '_revert' in request.POST:
            application.state = 'draft'
            application.save()

            messages.success(request,
                'The application was successfully reverted.')

        return redirect('admin:applications:index')

    context = {'application': application}
    if not application.call:
        context.update({'form': AssignCallForm(application)})

    if application.facility.name == 'SAAO':
        template_name = 'admin/applications/saao.html'
    elif application.facility.name == 'LCO':
        template_name = 'admin/applications/lcogt.html'
    elif application.facility.name == 'KMTNET':
        template_name = 'admin/applications/kmtnet.html'

    return render(request, template_name, context)


@login_required
def approve(request, pk):
    if not (request.user.is_staff and
            request.user.has_perm('portal.approve_application')):
        raise PermissionDenied

    application = get_object_or_404(Application, pk=pk,
        state='submitted')
    if request.method == 'POST':
        form = EmailForm(request.POST)
        if '_notify' in request.POST and form.is_valid():
            application.approve()
            application.save()

            # Get coordinators so the we can BCC them for auditing purposes.
            coordinators = Group.objects.get(name='Coordinators').user_set.all()

            form.send(
                from_='observer@saao.ac.za',
                bcc=[c.email for c in coordinators],
            )
            messages.success(request, 'The observer has been notified of the update.')
            return redirect('admin:applications:index')
        elif '_notify' not in request.POST:
            application.approve()
            application.save()
            messages.success(request, 'The application has been approved.')
            return redirect('admin:applications:index')
    else:
        message = render_to_string('applications/saao/emails/approved.txt', {
            'application': application
        })
        form = EmailForm(initial={
            'to': application.email,
            'cc': ','.join(a.email for a in application.applicants.all()
                if a.email != application.email),
            'subject': '[{}] Telescope Time Application'.format(
                application),
            'message': message,
        })
    return render(request, 'admin/applications/approve.html', {
        'application': application, 'form': form})


@login_required
def reject(request, pk):
    if not (request.user.is_staff and
            request.user.has_perm('portal.reject_application')):
        raise PermissionDenied

    application = get_object_or_404(Application, pk=pk,
        state='submitted')
    if request.method == 'POST':
        form = EmailForm(request.POST)
        if '_notify' in request.POST and form.is_valid():
            application.reject()
            application.save()
            form.send(from_='observer@saao.ac.za')
            messages.success(request, 'The observer has been notified of the update.')
            return redirect('admin:applications:index')
        elif '_notify' not in request.POST:
            application.reject()
            application.save()
            messages.success(request, 'The application has been rejected.')
            return redirect('admin:applications:index')
    else:
        message = render_to_string('applications/saao/emails/rejected.txt', {
            'application': application
        })
        form = EmailForm(initial={
            'to': application.email,
            'cc': ','.join(a.email for a in application.applicants.all()
                if a.email != application.email),
            'subject': '[{}] Telescope Time Application'.format(
                application),
            'message': message,
        })
    return render(request, 'admin/applications/reject.html', {
        'application': application, 'form': form})


@login_required
def download(request, reference):
    if not (request.user.is_staff and
            request.user.has_perm('portal.view_application')):
        raise PermissionDenied

    application = get_object_or_404(Application, reference=reference)

    response = HttpResponse(content_type='applicaiton/pdf')
    response['Content-Disposition'] = 'attachment; filename="{}.pdf"'.format(application)

    buffer = application.as_pdf()
    pdf = buffer.getvalue()
    response.write(pdf)

    return response


@login_required
def rota(request):
    """Exports a 'ROTA' in CSV format."""

    if not (request.user.is_staff and
            request.user.has_perm('portal.view_application')):
        raise PermissionDenied

    applications = Application.objects.exclude(state='draft')
    if 'state' in request.GET:
        applications = applications.filter(state=request.GET['state'])
    if 'call' in request.GET:
        applications = applications.filter(call=request.GET['call'])

    response = HttpResponse(content_type='text/plain')
    response['Content-Disposition'] = 'attachment; filename="rota.csv"'

    writer = csv.writer(response)
    writer.writerow([
        'Week',  # This will be populated manually by an administrator.
        'Trimester',
        'Observers',
        'Principal Investigator',
        'Telescope',
        'Instrument',
        'Gratings/Filters',
        'Impossible Dates',
        'Requires Support',
    ])
    for application in applications:
        if application.is_support_required == True:
            requires_support = 'Yes'
        elif application.is_support_required == False:
            requires_support = 'No'
        else:
            # This field was added later and defaults to NULL so we can't know
            # whether they required support or not.
            requires_support = '-'

        writer.writerow([
            '',
            application.call,
            '/'.join(map(str, application.observers)),
            ' '.join([application.first_name.encode('utf-8'), application.last_name.encode('utf-8')]),
            application.telescope,
            application.instrument,
            application.gratings_filters.encode('utf-8'),
            application.dates_impossible,
            requires_support,
        ])

    return response
