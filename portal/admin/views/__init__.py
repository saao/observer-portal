from django.shortcuts import render, redirect
from django.core.exceptions import PermissionDenied
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from portal.admin.forms import EmailForm, CallForProposalsForm
from . import applications
from . import telescopes
from . import instruments
from . import facilities
from . import calls
from . import departments
from . import users
from . import groups
from . import sites
from . import reports


@login_required
def call_for_proposals(request):
    """Send out and create a new call for proposals."""

    if not (request.user.is_staff and
            request.user.has_perm('portal.add_call')):
        raise PermissionDenied

    if request.method == 'POST':
        form = CallForProposalsForm(request.POST)
        email_form = EmailForm(request.POST)
        if form.is_valid() and email_form.is_valid():
            call = form.save(commit=False)
            call.created_by = request.user
            call.save()
            email_form.send(request.user.email)
            messages.success(request, 'The observers have been notified of the new call for proposals.')
            return redirect('admin:applications:index')
    else:
        form = CallForProposalsForm()
        email_form = EmailForm(initial={
            'to': 'saastronomers@saao.ac.za',
            'cc': 'stuc@saao.ac.za,intlobservers@saao.ac.za,postgrad@saao.ac.za,observer@saao.ac.za',
        })

    return render(request, 'admin/call_for_proposals.html', {
        'form': form, 'email_form': email_form})


@login_required
def calendar(request):
    if not request.user.is_staff:
        raise PermissionDenied
    return render(request, 'admin/calendar.html')
