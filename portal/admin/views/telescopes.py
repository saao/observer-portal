from django import forms
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.decorators import method_decorator
from django.forms.models import modelform_factory
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView
from portal.admin.decorators import user_is_staff
from portal.models import Telescope, Facility


decorators = [
    login_required,
    user_is_staff,

    # We're intentionally using a non-existent permission so that only
    # superusers have access.
    permission_required('portal.manage_telescopes', raise_exception=True)
]


@method_decorator(decorators, name='dispatch')
class TelescopeListView(ListView):
    model = Telescope
    template_name = 'admin/telescopes/index.html'
    context_object_name = 'telescopes'

    def get_context_data(self, **kwargs):
        context = super(TelescopeListView, self).get_context_data(**kwargs)
        context['facilities'] = Facility.objects.all()
        return context

    def get_queryset(self):
        if 'facility' in self.request.GET:
            try:
                facility = Facility.objects.get(pk=self.request.GET['facility'])
            except Facility.DoesNotExist:
                telescopes = []
            else:
                telescopes = facility.telescopes.all()
        else:
            telescopes = Telescope.objects.all()
        return telescopes
index = TelescopeListView.as_view()


@method_decorator(decorators, name='dispatch')
class TelescopeAddView(CreateView):
    model = Telescope
    template_name = 'admin/telescopes/add.html'
    fields = ('name', 'instruments', 'is_available', 'status',)
    labels = {
        'is_available': 'Available',
        'status': 'Status Message',
    }
    widgets = {
        'name': forms.TextInput(attrs={'class': 'form-control'}),
        'instruments': forms.CheckboxSelectMultiple(),
        'status': forms.TextInput(attrs={
            'class': 'form-control',
            'placeholder': 'A reason why the telescope is not available'
        }),
    }

    def get_form_class(self):
        return modelform_factory(self.model, fields=self.fields,
            labels=self.labels, widgets=self.widgets)

    def form_valid(self, form):
        messages.success(self.request, 'The telescope was added successfully.')
        return super(TelescopeAddView, self).form_valid(form)

    def get_success_url(self):
        if '_continue' in self.request.POST:
            return reverse('admin:telescopes:edit', kwargs={'pk': self.object.pk})
        return reverse('admin:telescopes:index')
add = TelescopeAddView.as_view()


@method_decorator(decorators, name='dispatch')
class TelescopeEditView(UpdateView):
    model = Telescope
    template_name = 'admin/telescopes/edit.html'
    fields = ('name', 'instruments', 'is_available', 'status',)
    labels = {
        'is_available': 'Available',
        'status': 'Status Message',
    }
    widgets = {
        'name': forms.TextInput(attrs={'class': 'form-control'}),
        'instruments': forms.CheckboxSelectMultiple(),
        'status': forms.TextInput(attrs={
            'class': 'form-control',
            'placeholder': 'A reason why the telescope is not available'
        }),
    }

    def get_form_class(self):
        return modelform_factory(self.model, fields=self.fields,
            labels=self.labels, widgets=self.widgets)

    def form_valid(self, form):
        messages.success(self.request, 'The telescope was updated successfully.')
        return super(TelescopeEditView, self).form_valid(form)

    def get_success_url(self):
        if '_continue' in self.request.POST:
            return reverse('admin:telescopes:edit', kwargs={'pk': self.object.pk})
        return reverse('admin:telescopes:index')
edit = TelescopeEditView.as_view()
