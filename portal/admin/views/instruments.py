from django import forms
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.decorators import method_decorator
from django.forms.models import modelform_factory
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView
from portal.admin.decorators import user_is_staff
from portal.models import Instrument, Telescope


decorators = [
    login_required,
    user_is_staff,

    # We're intentionally using a non-existent permission so that only
    # superusers have access.
    permission_required('portal.manage_instruments', raise_exception=True)
]


@method_decorator(decorators, name='dispatch')
class InstrumentListView(ListView):
    model = Instrument
    template_name = 'admin/instruments/index.html'
    context_object_name = 'instruments'

    def get_context_data(self, **kwargs):
        context = super(InstrumentListView, self).get_context_data(**kwargs)
        context['telescopes'] = Telescope.objects.all()
        return context

    def get_queryset(self):
        if 'telescope' in self.request.GET:
            try:
                telescope = Telescope.objects.get(pk=self.request.GET['telescope'])
            except Telescope.DoesNotExist:
                instruments = []
            else:
                instruments = telescope.instruments.all()
        else:
            instruments = Instrument.objects.all()
        return instruments
index = InstrumentListView.as_view()


@method_decorator(decorators, name='dispatch')
class InstrumentAddView(CreateView):
    model = Instrument
    template_name = 'admin/instruments/add.html'
    fields = ('name', 'is_available', 'status',)
    labels = {
        'is_available': 'Available',
        'status': 'Status Message',
    }
    widgets = {
        'name': forms.TextInput(attrs={'class': 'form-control'}),
        'status': forms.TextInput(attrs={
            'class': 'form-control',
            'placeholder': 'A reason why the instrument is not available'
        }),
    }

    def get_form_class(self):
        return modelform_factory(self.model, fields=self.fields,
            labels=self.labels, widgets=self.widgets)

    def form_valid(self, form):
        messages.success(self.request, 'The instrument was added successfully.')
        return super(InstrumentAddView, self).form_valid(form)

    def get_success_url(self):
        if '_continue' in self.request.POST:
            return reverse('admin:instruments:edit', kwargs={'pk': self.object.pk})
        return reverse('admin:instruments:index')
add = InstrumentAddView.as_view()


@method_decorator(decorators, name='dispatch')
class InstrumentEditView(UpdateView):
    model = Instrument
    template_name = 'admin/instruments/edit.html'
    fields = ('name', 'is_available', 'status',)
    labels = {
        'is_available': 'Available',
        'status': 'Status Message',
    }
    widgets = {
        'name': forms.TextInput(attrs={'class': 'form-control'}),
        'status': forms.TextInput(attrs={
            'class': 'form-control',
            'placeholder': 'A reason why the instrument is not available'
        }),
    }

    def get_form_class(self):
        return modelform_factory(self.model, fields=self.fields,
            labels=self.labels, widgets=self.widgets)

    def form_valid(self, form):
        messages.success(self.request, 'The instrument was updated successfully.')
        return super(InstrumentEditView, self).form_valid(form)

    def get_success_url(self):
        if '_continue' in self.request.POST:
            return reverse('admin:instruments:edit', kwargs={'pk': self.object.pk})
        return reverse('admin:instruments:index')
edit = InstrumentEditView.as_view()
