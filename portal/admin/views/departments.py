from django import forms
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.decorators import method_decorator
from django.forms.models import modelform_factory
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView
from portal.admin.decorators import user_is_staff
from portal.models import Department


decorators = [
    login_required,
    user_is_staff,

    # We're intentionally using a non-existent permission so that only
    # superusers can access the department admin.
    permission_required('portal.manage_departments', raise_exception=True)
]


@method_decorator(decorators, name='dispatch')
class DepartmentListView(ListView):
    model = Department
    template_name = 'admin/departments/index.html'
    context_object_name = 'departments'
index = DepartmentListView.as_view()


@method_decorator(decorators, name='dispatch')
class DepartmentCreateView(CreateView):
    model = Department
    template_name = 'admin/departments/create.html'
    fields = ('name', 'parent', 'head',)
    widgets = {
        'name': forms.TextInput(attrs={'class': 'form-control'}),
        'parent': forms.Select(attrs={'class': 'form-control'}),
        'head': forms.Select(attrs={'class': 'form-control'}),
    }

    def get_form_class(self):
        return modelform_factory(self.model, fields=self.fields,
            widgets=self.widgets)

    def form_valid(self, form):
        messages.success(self.request, 'The department was added successfully.')
        return super(DepartmentCreateView, self).form_valid(form)

    def get_success_url(self):
        if '_continue' in self.request.POST:
            return reverse('admin:departments:update', kwargs={'pk': self.object.pk})
        return reverse('admin:departments:index')
create = DepartmentCreateView.as_view()


@method_decorator(decorators, name='dispatch')
class DepartmentUpdateView(UpdateView):
    model = Department
    template_name = 'admin/departments/update.html'
    fields = ('name', 'parent', 'head',)
    widgets = {
        'name': forms.TextInput(attrs={'class': 'form-control'}),
        'parent': forms.Select(attrs={'class': 'form-control'}),
        'head': forms.Select(attrs={'class': 'form-control'}),
    }

    def get_form_class(self):
        return modelform_factory(self.model, fields=self.fields,
            widgets=self.widgets)

    def form_valid(self, form):
        messages.success(self.request, 'The department was updated successfully.')
        return super(DepartmentUpdateView, self).form_valid(form)

    def get_success_url(self):
        if '_continue' in self.request.POST:
            return reverse('admin:departments:update', kwargs={'pk': self.object.pk})
        return reverse('admin:departments:index')
update = DepartmentUpdateView.as_view()
