from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.decorators import method_decorator
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView
from portal.admin.decorators import user_is_staff
from portal.admin.forms import CallForProposalsForm
from portal.models import Call, Facility


decorators = [
    login_required,
    user_is_staff,
    permission_required('portal.add_call', raise_exception=True)
]


@method_decorator(decorators, name='dispatch')
class CallListView(ListView):
    model = Call
    template_name = 'admin/calls/index.html'
    context_object_name = 'calls'

    def get_context_data(self, **kwargs):
        context = super(CallListView, self).get_context_data(**kwargs)
        context['facilities'] = Facility.objects.all()
        return context

    def get_queryset(self):
        if 'facility' in self.request.GET:
            try:
                facility = Facility.objects.get(pk=self.request.GET['facility'])
            except Facility.DoesNotExist:
                calls = []
            else:
                calls = facility.calls.all()
        else:
            calls = Call.objects.all()
        return calls
index = CallListView.as_view()


@method_decorator(decorators, name='dispatch')
class CallAddView(CreateView):
    model = Call
    template_name = 'admin/calls/add.html'
    form_class = CallForProposalsForm

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        messages.success(self.request, 'The call was added successfully.')
        return super(CallAddView, self).form_valid(form)

    def get_success_url(self):
        if '_continue' in self.request.POST:
            return reverse('admin:calls:edit', kwargs={'pk': self.object.pk})
        return reverse('admin:calls:index')
add = CallAddView.as_view()


@method_decorator(decorators, name='dispatch')
class CallEditView(UpdateView):
    model = Call
    template_name = 'admin/calls/edit.html'
    form_class = CallForProposalsForm

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if 'delete' in request.POST:
            if len(self.object.applications.all()) > 0:
                messages.error(request, 'The call cannot be deleted. It has '
                    'applications associated with it.')
                return HttpResponseRedirect(
                    reverse('admin:calls:edit', kwargs={'pk': self.object.pk}))
            self.object.delete()
            messages.success(request, 'The call was deleted.')
            return HttpResponseRedirect(self.get_success_url())
        return super(CallEditView, self).post(request, *args, **kwargs)

    def form_valid(self, form):
        messages.success(self.request, 'The call was updated successfully.')
        return super(CallEditView, self).form_valid(form)

    def get_success_url(self):
        if '_continue' in self.request.POST:
            return reverse('admin:calls:edit', kwargs={'pk': self.object.pk})
        return reverse('admin:calls:index')
edit = CallEditView.as_view()
