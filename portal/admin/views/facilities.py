from django import forms
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.forms.models import modelform_factory
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView
from portal.admin.decorators import user_is_staff
from portal.models import Facility


decorators = [login_required, user_is_staff]


@method_decorator(decorators, name='dispatch')
class FacilityListView(ListView):
    model = Facility
    template_name = 'admin/facilities/index.html'
    context_object_name = 'facilities'
index = FacilityListView.as_view()


@method_decorator(decorators, name='dispatch')
class FacilityAddView(CreateView):
    model = Facility
    template_name = 'admin/facilities/add.html'
    fields = ('name', 'telescopes',)
    widgets = {
        'name': forms.TextInput(attrs={
            'class': 'form-control',
        }),
        'telescopes': forms.CheckboxSelectMultiple(),
    }

    def get_form_class(self):
        return modelform_factory(self.model, fields=self.fields, widgets=self.widgets)

    def form_valid(self, form):
        messages.success(self.request, 'The facility was added successfully.')
        return super(FacilityAddView, self).form_valid(form)

    def get_success_url(self):
        if '_continue' in self.request.POST:
            return reverse('admin:facilities:edit', kwargs={'pk': self.object.id})
        return reverse('admin:facilities:index')
add = FacilityAddView.as_view()


@method_decorator(decorators, name='dispatch')
class FacilityEditView(UpdateView):
    model = Facility
    template_name = 'admin/facilities/edit.html'
    fields = ('name', 'telescopes',)
    widgets = {
        'name': forms.TextInput(attrs={
            'class': 'form-control',
        }),
        'telescopes': forms.CheckboxSelectMultiple(),
    }

    def get_form_class(self):
        return modelform_factory(self.model, fields=self.fields, widgets=self.widgets)

    def form_valid(self, form):
        messages.success(self.request, 'The facility was updated successfully.')
        return super(FacilityEditView, self).form_valid(form)

    def get_success_url(self):
        if '_continue' in self.request.POST:
            return reverse('admin:facilities:edit', kwargs={'pk': self.object.id})
        return reverse('admin:facilities:index')
edit = FacilityEditView.as_view()
