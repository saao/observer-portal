from django import forms
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.contrib.auth.models import Group
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.forms.models import modelform_factory
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView
from portal.admin.decorators import user_is_staff


decorators = [login_required, user_is_staff]


@method_decorator(decorators, name='dispatch')
class GroupListView(ListView):
    model = Group
    template_name = 'admin/groups/index.html'
    context_object_name = 'groups'
index = GroupListView.as_view()


@method_decorator(decorators, name='dispatch')
class GroupAddView(CreateView):
    model = Group
    template_name = 'admin/groups/add.html'
    fields = ('name', 'permissions',)
    widgets = {
        'name': forms.TextInput(attrs={'class': 'form-control'}),
        'permissions': forms.CheckboxSelectMultiple(),
    }

    def get_form_class(self):
        return modelform_factory(self.model, fields=self.fields, widgets=self.widgets)

    def form_valid(self, form):
        messages.success(self.request, 'The group was added successfully.')
        return super(GroupAddView, self).form_valid(form)

    def get_success_url(self):
        if '_continue' in self.request.POST:
            return reverse('admin:groups:edit', kwargs={'pk': self.object.pk})
        return reverse('admin:groups:index')
add = GroupAddView.as_view()


@method_decorator(decorators, name='dispatch')
class GroupEditView(UpdateView):
    model = Group
    template_name = 'admin/groups/edit.html'
    fields = ('name', 'permissions',)
    widgets = {
        'name': forms.TextInput(attrs={'class': 'form-control'}),
        'permissions': forms.CheckboxSelectMultiple(),
    }

    def get_form_class(self):
        return modelform_factory(self.model, fields=self.fields, widgets=self.widgets)

    def form_valid(self, form):
        messages.success(self.request, 'The group was updated successfully.')
        return super(GroupEditView, self).form_valid(form)

    def get_success_url(self):
        if '_continue' in self.request.POST:
            return reverse('admin:groups:edit', kwargs={'pk': self.object.pk})
        return reverse('admin:groups:index')
edit = GroupEditView.as_view()
