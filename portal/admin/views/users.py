from django import forms
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.forms.models import modelform_factory
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView
from portal.forms import UserProfileForm


class UserListView(LoginRequiredMixin, UserPassesTestMixin, ListView):
    model = get_user_model()
    template_name = 'admin/users/index.html'
    context_object_name = 'users'
    raise_exception = True

    def test_func(self):
        return self.request.user.is_staff and self.request.user.is_superuser

    def get_context_data(self, **kwargs):
        context = super(UserListView, self).get_context_data(**kwargs)
        context['groups'] = Group.objects.all()
        return context

    def get_queryset(self):
        if 'group' in self.request.GET:
            try:
                group = Group.objects.get(pk=self.request.GET['group'])
            except Group.DoesNotExist:
                users = []
            else:
                users = group.user_set.all()
        else:
            users = self.model.objects.all()
            if 'is_staff' in self.request.GET:
                users = users.filter(is_staff=True)
            if 'is_superuser' in self.request.GET:
                users = users.filter(is_superuser=True)
        return users
index = UserListView.as_view()


class UserAddView(LoginRequiredMixin, UserPassesTestMixin, CreateView):
    model = get_user_model()
    template_name = 'admin/users/add.html'
    fields = ('email', 'first_name', 'last_name', 'groups', 'is_active',
        'is_staff', 'is_superuser', 'user_permissions',)
    labels = {
        'first_name': 'First Name',
        'last_name': 'Last Name',
        'is_active': 'Active',
        'is_staff': 'Staff Member',
        'is_superuser': 'Administrator',
    }
    widgets = {
        'email': forms.EmailInput(attrs={'class': 'form-control'}),
        'first_name': forms.TextInput(attrs={'class': 'form-control'}),
        'last_name': forms.TextInput(attrs={'class': 'form-control'}),
        'groups': forms.CheckboxSelectMultiple(),
        'user_permissions': forms.SelectMultiple(attrs={'class': 'form-control', 'size': 15}),
    }
    raise_exception = True

    def test_func(self):
        return self.request.user.is_staff and self.request.user.is_superuser

    def get_form_class(self):
        return modelform_factory(self.model, fields=self.fields,
            labels=self.labels, widgets=self.widgets)

    def form_valid(self, form):
        messages.success(self.request, 'The user was added successfully.')
        return super(UserAddView, self).form_valid(form)

    def get_success_url(self):
        if '_continue' in self.request.POST:
            return reverse('admin:users:edit', kwargs={'pk': self.object.pk})
        return reverse('admin:users:index')
add = UserAddView.as_view()


class UserEditView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    model = get_user_model()
    template_name = 'admin/users/edit.html'
    fields = ('email', 'first_name', 'last_name', 'groups', 'is_active',
        'is_staff', 'is_superuser', 'user_permissions',)
    labels = {
        'first_name': 'First Name',
        'last_name': 'Last Name',
        'is_active': 'Active',
        'is_staff': 'Staff Member',
        'is_superuser': 'Administrator',
    }
    widgets = {
        'email': forms.EmailInput(attrs={'class': 'form-control'}),
        'first_name': forms.TextInput(attrs={'class': 'form-control'}),
        'last_name': forms.TextInput(attrs={'class': 'form-control'}),
        'groups': forms.CheckboxSelectMultiple(),
        'user_permissions': forms.SelectMultiple(attrs={'class': 'form-control', 'size': 15}),
    }
    raise_exception = True

    def test_func(self):
        return self.request.user.is_staff and self.request.user.is_superuser

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = self.get_form()
        profile_form = UserProfileForm(self.request.POST,
                self.request.FILES,
                instance=self.object.profile)
        if form.is_valid() and profile_form.is_valid():
            profile_form.save()
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_form_class(self):
        return modelform_factory(self.model, fields=self.fields,
            labels=self.labels, widgets=self.widgets)

    def get_context_data(self, **kwargs):
        context = super(UserEditView, self).get_context_data(**kwargs)
        if 'profile_form' not in context:
            user = self.object
            kwargs = {
                'instance': user.profile,
            }
            if self.request.method in ('POST', 'PUT'):
                kwargs['data'] = self.request.POST
            context['profile_form'] = UserProfileForm(**kwargs)
        return context

    def form_valid(self, form):
        messages.success(self.request, 'The user was updated successfully.')
        return super(UserEditView, self).form_valid(form)

    def get_success_url(self):
        if '_continue' in self.request.POST:
            return reverse('admin:users:edit', kwargs={'pk': self.object.pk})
        return reverse('admin:users:index')
edit = UserEditView.as_view()
