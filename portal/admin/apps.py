"""Define a unique name and label for the app that doesn't clash with Django's admin app."""
from django.apps import AppConfig


class AdminConfig(AppConfig):
    name = 'portal.admin'
    label = 'portal_admin'
