from django.core.exceptions import PermissionDenied


def user_is_staff(f):
    def wrapped_view(request, *args, **kwargs):
        if not request.user.is_staff:
            raise PermissionDenied
        return f(request, *args, **kwargs)
    return wrapped_view


def user_is_superuser(f):
    def wrapped_view(request, *args, **kwargs):
        if not request.user.is_superuser:
            raise PermissionDenied
        return f(request, *args, **kwargs)
    return wrapped_view
