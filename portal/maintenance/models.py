import datetime

from django.db import models
from django.conf import settings
from django.contrib.sites.models import Site
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible


def get_default_site_id():
    return settings.SITE_ID


@python_2_unicode_compatible
class Category(models.Model):
    """Represents a type of item that can be worked on."""

    class Meta:
        db_table = 'maintenance_categories'
        ordering = ['order', 'name']

    name = models.CharField(max_length=255)
    site = models.ForeignKey(Site, on_delete=models.CASCADE, default=get_default_site_id)

    # An item can optionally be linked to a telescope. We might have to change
    # this into a generic foreign key at some point so we can link items to
    # instruments too.
    telescope = models.OneToOneField('portal.Telescope', null=True, blank=True, default=None,
        on_delete=models.SET_DEFAULT)
    order = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.name

    def daylog(self):
        """Returns logs for the last 24 hours."""

        return self.logs.filter(
            created_at__gt=timezone.now() - datetime.timedelta(days=1))


class Log(models.Model):
    """A log containing a description of work done on an item."""

    class Meta:
        db_table = 'maintenance_logs'
        ordering = ['-created_at']

    comment = models.TextField('Comment')
    category = models.ForeignKey('Category', related_name='logs',
        on_delete=models.PROTECT)
    standby = models.BooleanField(default=False)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL,
        on_delete=models.PROTECT)
    created_at = models.DateTimeField(auto_now_add=True)
