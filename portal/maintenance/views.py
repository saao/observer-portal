from datetime import datetime
from django.shortcuts import render, redirect, get_object_or_404
from django.conf import settings
from django.contrib import messages
from django.core.exceptions import PermissionDenied
from django.contrib.auth.decorators import login_required
from .models import Log, Category
from .forms import LogItemForm, LogForm


@login_required
def index(request):
    if not request.user.is_staff:
        raise PermissionDenied

    try:
        start_date = datetime.strptime(request.GET.get('start'), '%Y-%m-%d')
        end_date = datetime.strptime(request.GET.get('end'), '%Y-%m-%d')
    except (TypeError, ValueError):
        now = datetime.utcnow()
        start_date, end_date = now, now

    context = {
        'categories': Category.objects.filter(site=settings.SITE_ID),
        'start_date': start_date,
        'end_date': end_date,
        'logs': Log.objects.filter(
            created_at__gte=start_date.replace(hour=0, minute=0, second=0),
            created_at__lte=end_date.replace(hour=23, minute=59, second=59,),
            category__site_id=settings.SITE_ID),
    }

    return render(request, 'index.html', context)


@login_required
def category(request, pk):
    """Displays a list of logs for the given category."""
    if not request.user.is_staff:
        raise PermissionDenied

    category = get_object_or_404(Category, pk=pk, site=settings.SITE_ID)

    if request.method == 'POST':
        form = LogItemForm(request.POST)
        if form.is_valid():
            form.save(created_by=request.user)
            messages.success(request, 'Thank you! Your worklog has been submitted.')
            return redirect('maintenance:category', pk=pk)
    else:
        form = LogItemForm(initial={
            'category_id': category.pk,
            'category_name': category.name,
        })

    context = {
        'form': form,
        'category': category,
        'categories': Category.objects.filter(site=settings.SITE_ID),
    }

    try:
        start_date = datetime.strptime(request.GET.get('start'), '%Y-%m-%d')
        end_date = datetime.strptime(request.GET.get('end'), '%Y-%m-%d')
    except (TypeError, ValueError):
        now = datetime.utcnow()
        start_date, end_date = now, now

    context.update({
        'start_date': start_date,
        'end_date': end_date,
        'logs': category.logs.filter(
            created_at__gte=start_date.replace(hour=0, minute=0, second=0),
            created_at__lte=end_date.replace(hour=23, minute=59, second=59)),
    })

    return render(request, 'category.html', context)


@login_required
def daylog(request):
    if not request.user.is_staff:
        raise PermissionDenied

    if request.method == 'POST':
        form = LogForm(request.POST)
        if form.is_valid() and form.log_items.is_valid():
            form.save(created_by=request.user)
            messages.success(request,
                'Thank you! Your worklog has been submitted.')
            return redirect('/')
    else:
        form = LogForm()

    return render(request, 'daylog.html', {'form': form})
