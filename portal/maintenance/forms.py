from django import forms
from django.conf import settings
from django.forms import formset_factory
from .models import Category, Log


class LogItemForm(forms.Form):
    category_id = forms.IntegerField()
    category_name = forms.CharField()
    comment = forms.CharField(widget=forms.Textarea(attrs={
        'class': 'form-control no-border',
        'placeholder': 'Please provide a brief description of the work.'}))

    def save(self, created_by):
        log = Log(comment=self.cleaned_data['comment'],
            category_id=self.cleaned_data['category_id'],
            created_by=created_by,)
        return log.save()


class OptionalLogItemForm(LogItemForm):
    comment = forms.CharField(required=False, widget=forms.Textarea(attrs={
        'class': 'form-control no-border',
        'placeholder': 'Leave empty if you have not done any work on this today.'}))


LogItemFormSet = formset_factory(OptionalLogItemForm, extra=0)


class LogForm(forms.Form):
    standby = forms.BooleanField(required=False)

    def __init__(self, data=None, *args, **kwargs):
        super(LogForm, self).__init__(data, *args, **kwargs)
        self.log_items = LogItemFormSet(data, initial=[{
            'category_id': c.pk, 'category_name': c.name} for c in Category.objects.filter(site=settings.SITE_ID)])

    def save(self, created_by):
        logs = []
        for form in self.log_items.forms:
            logs.append(
                Log(comment=form.cleaned_data['comment'] or 'No work was done today.',
                    category_id=form.cleaned_data['category_id'],
                    standby=self.cleaned_data.get('standby', False),
                    created_by=created_by,)
            )
        Log.objects.bulk_create(logs)
