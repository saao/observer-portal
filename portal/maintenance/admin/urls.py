from django.conf.urls import include, url
from . import views


urlpatterns = [
    url(r'^categories/', include([
        url(r'^$', views.index, name='index'),
        url(r'^new$', views.create, name='create'),
        url(r'^(?P<pk>\d+)$', views.update, name='update'),
    ], namespace='categories'))
]
