from django import forms
from django.conf import settings
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.forms.models import modelform_factory
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView
from portal.admin.decorators import user_is_staff
from ..models import Category


decorators = [login_required, user_is_staff]


@method_decorator(decorators, name='dispatch')
class CategoryListView(ListView):
    model = Category
    template_name = 'admin/index.html'
    context_object_name = 'categories'

    def get_queryset(self):
        return Category.objects.filter(site=settings.SITE_ID)
index = CategoryListView.as_view()


@method_decorator(decorators, name='dispatch')
class CategoryCreateView(CreateView):
    model = Category
    template_name = 'admin/create.html'
    fields = ('name',)
    widgets = {
        'name': forms.TextInput(attrs={'class': 'form-control'}),
    }

    def get_form_class(self):
        return modelform_factory(self.model, fields=self.fields, widgets=self.widgets)

    def form_valid(self, form):
        messages.success(self.request, 'The category was added successfully.')
        return super(CategoryCreateView, self).form_valid(form)

    def get_success_url(self):
        if '_continue' in self.request.POST:
            return reverse('admin:categories:update', kwargs={'pk': self.object.pk})
        return reverse('admin:categories:index')
create = CategoryCreateView.as_view()


@method_decorator(decorators, name='dispatch')
class CategoryUpdateView(UpdateView):
    model = Category
    template_name = 'admin/update.html'
    fields = ('name',)
    widgets = {
        'name': forms.TextInput(attrs={'class': 'form-control'}),
    }

    def get_form_class(self):
        return modelform_factory(self.model, fields=self.fields, widgets=self.widgets)

    def form_valid(self, form):
        messages.success(self.request, 'The category was updated successfully.')
        return super(CategoryUpdateView, self).form_valid(form)

    def get_success_url(self):
        if '_continue' in self.request.POST:
            return reverse('admin:categories:update', kwargs={'pk': self.object.pk})
        return reverse('admin:categories:index')
update = CategoryUpdateView.as_view()
