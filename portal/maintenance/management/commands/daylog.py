"""Sends an email containing all maintenance logs for the day."""


import datetime

from django.conf import settings
from django.core.management.base import BaseCommand
from django.core.mail import EmailMessage
from django.contrib.sites.models import Site
from django.template.loader import render_to_string
from portal.maintenance.models import Category


class Command(BaseCommand):
    help = "Send out today's maintenance log"

    def add_arguments(self, parser):
        parser.add_argument('recipients', nargs='*', type=str, default=[
            email for name, email in settings.MANAGERS])

    def handle(self, *args, **options):
        message = render_to_string('emails/daylog.txt', {
            'categories': Category.objects.filter(site=settings.SITE_ID),
            'site': Site.objects.get(pk=settings.SITE_ID),
        })
        email = EmailMessage('Day Log {:%d %h %Y}'.format(datetime.date.today()),
            message, settings.DEFAULT_FROM_EMAIL, options['recipients'])
        email.send()
