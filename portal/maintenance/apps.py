from django.apps import AppConfig


class MaintenanceConfig(AppConfig):
    name = 'portal.maintenance'
    label = 'portal_maintenance'
