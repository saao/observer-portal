from django.conf.urls import include, url
from . import views


urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^(?P<pk>\d+)$', views.category, name='category'),
    url(r'^daylog/$', views.daylog, name='daylog'),

    url(r'^', include('portal.maintenance.admin.urls')),
]
