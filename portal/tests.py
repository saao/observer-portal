# -*- coding: utf-8 -*-

from __future__ import unicode_literals

import os
import mock

from django.conf import settings
from django.test import TransactionTestCase, override_settings
from django.core.urlresolvers import reverse
from django.contrib.auth import get_user_model
from .models import Application


User = get_user_model()


APPLICATION = {
    'title': 'Test Application',
    'abstract': 'abstract',
    'abstract_public': 'public abstract',
    'telescope': 2,
    'instrument': 6,
    'facility': 1,
    'gratings_filters': 'grating 4',
    'snr': 0,
    'magnitude_range_min': 0.0,
    'magnitude_range_max': 0.0,
    'magnitude_range_filter': 'V',
    'time_dark': 0.0,
    'time_grey': 0.0,
    'time_bright': 0.0,
    'time_min_dark': 0.0,
    'time_min_bright': 0.0,
    'time_min_grey': 0.0,
    'time_additional_dark': 0.0,
    'time_additional_bright': 0.0,
    'time_additional_grey': 0.0,
    'time_grey_moon_quarter': 3,
    'required_support': 'none',
    'targets-TOTAL_FORMS': 1,
    'targets-INITIAL_FORMS': 0,
    'targets-0-declination': 'dec',
    'targets-0-comment': 'comment',
    'targets-0-right_ascension': 'ra',
    'targets-0-name': 'target',
    'targets-0-vmagnitude': 1.0,
    'other_applications-TOTAL_FORMS': 1,
    'other_applications-INITIAL_FORMS': 0,
    'previous_applications-TOTAL_FORMS': 1,
    'previous_applications-INITIAL_FORMS': 0,
    'applicants-TOTAL_FORMS': 1,
    'applicants-INITIAL_FORMS': 0,
    'applicants-0-last_name': 'Lombaard',
    'applicants-0-institution': 'test',
    'applicants-0-first_name': 'Briehan',
    'applicants-0-role': 'applicant',
    'applicants-0-email': 'briehan@saao.ac.za',
    'applicants-0-is_observer': 'on',
    'first_name': 'first',
    'last_name': 'last',
    'street': 'street1',
    'city': 'city',
    'postal_code': 'postal code',
    'country': 'ZA',
    'phone': '1234',
    'email': 'first.last@example.com',
}


@override_settings(MEDIA_ROOT=os.path.join(settings.BASE_DIR, 'media'))
class ViewTests(TransactionTestCase):
    fixtures = ['portal/fixtures/initial_data.json']

    def test_list_applications(self):
        user = User.objects.create(
            username='username', email='username@example.com', is_active=True)
        user.set_password('password')
        user.save()

        with mock.patch('ldap3.Connection'):
            self.client.login(username='username@example.com', password='password')

        response = self.client.get(reverse('applications:index'))
        self.assertTrue(response.status_code==200)

    def test_add_draft_application(self):
        user = User.objects.create(
            username='username', email='username@example.com', is_active=True)
        user.set_password('password')
        user.save()

        with mock.patch('ldap3.Connection'):
            self.client.login(username='username@example.com', password='password')

        response = self.client.get(reverse('applications:create', kwargs={
            'facility': 'SAAO'}))
        self.assertTrue(response.status_code==200)

        response = self.client.post(reverse('applications:create', kwargs={
            'facility': 'SAAO'}), {
            'draft': '',
            'title': 'Untitled',
            'telescope': 2,
            'facility': 1,

            'targets-TOTAL_FORMS': 1,
            'targets-INITIAL_FORMS': 0,

            'other_applications-TOTAL_FORMS': 1,
            'other_applications-INITIAL_FORMS': 0,

            'previous_applications-TOTAL_FORMS': 1,
            'previous_applications-INITIAL_FORMS': 0,

            'applicants-TOTAL_FORMS': 1,
            'applicants-INITIAL_FORMS': 0,

            'applicants-0-id': '',
            'applicants-0-last_name': '',
            'applicants-0-first_name': '',
            'applicants-0-email': '',
            'applicants-0-institution': '',
            'applicants-0-role': 'applicant',

            'time_additional_grey': 0,
            'time_additional_bright': 0,
            'time_additional_dark': 0,
        }, follow=True)
        self.assertRedirects(response, reverse('applications:index'))
        self.assertContains(response, 'Your application has been saved.')

    def test_edit_draft_application(self):
        user = User.objects.create(
            username='username', email='username@example.com', is_active=True)
        user.set_password('password')
        user.save()

        with mock.patch('ldap3.Connection'):
            self.client.login(username='username@example.com', password='password')

        application = Application()
        application.created_by = user
        application.facility_id = 1
        application.save()

        response = self.client.get(reverse('applications:update', kwargs={
            'pk': application.pk,
        }))
        self.assertTrue(response.status_code==200)

        response = self.client.post(reverse('applications:update', kwargs={
            'pk': application.pk
        }), {
            'draft': '',
            'title': 'Untitled',
            'telescope': '',
            'facility': 1,

            'targets-TOTAL_FORMS': 1,
            'targets-INITIAL_FORMS': 0,

            'other_applications-TOTAL_FORMS': 1,
            'other_applications-INITIAL_FORMS': 0,

            'previous_applications-TOTAL_FORMS': 1,
            'previous_applications-INITIAL_FORMS': 0,

            'applicants-TOTAL_FORMS': 1,
            'applicants-INITIAL_FORMS': 0,

            'applicants-0-id': '',
            'applicants-0-last_name': '',
            'applicants-0-first_name': '',
            'applicants-0-email': '',
            'applicants-0-institution': '',
            'applicants-0-role': 'applicant',

            'time_additional_grey': 0,
            'time_additional_bright': 0,
            'time_additional_dark': 0,
        }, follow=True)
        self.assertRedirects(response, reverse('applications:index'))
        self.assertContains(response, 'Your application has been saved.')

    def test_delete_draft_application(self):
        user = User.objects.create(
            username='username', email='username@example.com', is_active=True)
        user.set_password('password')
        user.save()

        with mock.patch('ldap3.Connection'):
            self.client.login(username='username@example.com', password='password')

        application = Application()
        application.created_by = user
        application.facility_id = 1
        application.save()

        response = self.client.post(reverse('applications:update', kwargs={
            'pk': application.pk,
        }), {
            'delete': '',
            'telescope': '',

            'targets-TOTAL_FORMS': 1,
            'targets-INITIAL_FORMS': 0,

            'other_applications-TOTAL_FORMS': 1,
            'other_applications-INITIAL_FORMS': 0,

            'previous_applications-TOTAL_FORMS': 1,
            'previous_applications-INITIAL_FORMS': 0,

            'applicants-TOTAL_FORMS': 1,
            'applicants-INITIAL_FORMS': 0,

            'applicants-0-id': '',
            'applicants-0-last_name': '',
            'applicants-0-first_name': '',
            'applicants-0-email': '',
            'applicants-0-institution': '',
            'applicants-0-role': 'applicant',
        }, follow=True)
        self.assertRedirects(response, reverse('applications:index'))
        self.assertContains(response, 'Your application was deleted.')

    def test_submit_draft_application(self):
        """Submit a draft application."""

        user = User.objects.create(
            username='username', email='username@example.com', is_active=True)
        user.set_password('password')
        user.save()

        with mock.patch('ldap3.Connection'):
            self.client.login(username='username@example.com', password='password')

        application = Application(title='Untitled')
        application.created_by = user
        application.facility_id = 1
        application.save()

        data = APPLICATION.copy()
        data.update({
            'submit': '',
            'scientific_justification': open(os.path.join(
                settings.MEDIA_ROOT, 'test.pdf')),
        })

        response = self.client.post(reverse('applications:update', kwargs={
            'pk': application.pk}), data, follow=True)
        self.assertRedirects(response, reverse('applications:submit', kwargs={
            'pk': application.pk}))
        self.assertContains(response, 'Please review the information you '
            'entered below and hit the submit button to submit your application.')

    def test_submit_application(self):
        """Submit an application without saving a draft first."""

        user = User.objects.create(
            username='username', email='username@example.com', is_active=True)
        user.set_password('password')
        user.save()

        with mock.patch('ldap3.Connection'):
            self.client.login(username='username@example.com', password='password')

        data = APPLICATION.copy()
        data.update({
            'submit': '',
            'scientific_justification': open(os.path.join(
                settings.MEDIA_ROOT, 'test.pdf')),
        })
        response = self.client.post(reverse('applications:create', kwargs={
            'facility': 'SAAO'}), data, follow=True)

        application = Application.objects.first()

        self.assertRedirects(response, reverse('applications:submit', kwargs={
            'pk': application.pk}))
        self.assertContains(response, 'Please review the information you '
            'entered below and hit the submit button to submit your application.')

    def test_submit_application_confirm(self):
        """Submit and confirm a draft application."""

        user = User.objects.create(
            username='username', email='username@example.com', is_active=True)
        user.set_password('password')
        user.save()

        with mock.patch('ldap3.Connection'):
            self.client.login(username='username@example.com', password='password')

        # Create a draft application first.
        data = APPLICATION.copy()
        data.update({
            'draft': '',
            'scientific_justification': open(os.path.join(
                settings.MEDIA_ROOT, 'test.pdf')),
        })
        response = self.client.post(reverse('applications:create', kwargs={
            'facility': 'SAAO'}), data, follow=True)

        application = Application.objects.first()

        # Then we can actually submit it on the confirmation page.
        response = self.client.post(reverse('applications:submit', kwargs={
            'pk': application.pk}), follow=True)
        self.assertRedirects(response, reverse('applications:update', kwargs={
            'pk': application.pk}))
        self.assertContains(response, 'Thank you for submitting your proposal! '
            'You should receive a confirmation email shortly.')

    def test_submit_application_with_spaces_in_reference(self):
        user = User.objects.create(
            username='username', email='username@example.com', is_active=True)
        user.set_password('password')
        user.save()

        with mock.patch('ldap3.Connection'):
            self.client.login(username='username@example.com', password='password')

        data = APPLICATION.copy()
        data.update({
            'last_name': 'de Cassini',  # Use a last name that contains a space.
            'submit': '',
            'scientific_justification': open(os.path.join(
                settings.MEDIA_ROOT, 'test.pdf')),
        })
        response = self.client.post(reverse('applications:create', kwargs={
            'facility': 'SAAO'}), data, follow=True)

        application = Application.objects.first()

        self.assertRedirects(response, reverse('applications:submit', kwargs={
            'pk': application.pk}))
        self.assertContains(response, 'Please review the information you '
            'entered below and hit the submit button to submit your application.')

        application = Application.objects.first()

        # Then we can actually submit it on the confirmation page.
        response = self.client.post(reverse('applications:submit', kwargs={
            'pk': application.pk}), follow=True)
        self.assertRedirects(response, reverse('applications:update', kwargs={
            'pk': application.pk}))
        self.assertContains(response, 'Thank you for submitting your proposal! '
            'You should receive a confirmation email shortly.')

        # We shoulnd't get any errors here.
        response = self.client.get(reverse('applications:index'))
        self.assertTrue(response.status_code==200)

    def test_submit_application_with_unicode_in_reference(self):
        user = User.objects.create(
            username='username', email='username@example.com', is_active=True)
        user.set_password('password')
        user.save()

        with mock.patch('ldap3.Connection'):
            self.client.login(username='username@example.com', password='password')

        data = APPLICATION.copy()
        data.update({
            'last_name': 'Ångström',  # Use a last name that contains unicode characters.
            'submit': '',
            'scientific_justification': open(os.path.join(
                settings.MEDIA_ROOT, 'test.pdf')),
        })
        response = self.client.post(reverse('applications:create', kwargs={
            'facility': 'SAAO'}), data, follow=True)

        application = Application.objects.first()

        self.assertRedirects(response, reverse('applications:submit', kwargs={
            'pk': application.pk}))
        self.assertContains(response, 'Please review the information you '
            'entered below and hit the submit button to submit your application.')

        application = Application.objects.first()

        # Then we can actually submit it on the confirmation page.
        response = self.client.post(reverse('applications:submit', kwargs={
            'pk': application.pk}), follow=True)
        self.assertRedirects(response, reverse('applications:update', kwargs={
            'pk': application.pk}))
        self.assertContains(response, 'Thank you for submitting your proposal! '
            'You should receive a confirmation email shortly.')

    def test_download_application(self):
        user = User.objects.create(
            username='username', email='username@example.com', is_active=True)
        user.set_password('password')
        user.save()

        with mock.patch('ldap3.Connection'):
            self.client.login(username='username@example.com', password='password')

        # Create a draft application first.
        data = APPLICATION.copy()
        data.update({
            'draft': '',
            'scientific_justification': open(os.path.join(
                settings.MEDIA_ROOT, 'test.pdf')),
        })
        response = self.client.post(reverse('applications:create', kwargs={
            'facility': 'SAAO'}), data, follow=True)

        application = Application.objects.first()

        response = self.client.post(reverse('applications:submit', kwargs={
            'pk': application.pk}), follow=True)

        # We need to get the application again to get the reference number.
        application = Application.objects.first()

        response = self.client.get(reverse('applications:download', kwargs={
            'reference': application.reference}))

        self.assertEqual(response['Content-Type'], 'application/pdf')
        self.assertEqual(response['Content-Disposition'],
            'attachment; filename="{}.pdf"'.format(application))

    def test_access_other_persons_application(self):
        """Users shouldn't be able to view or edit other people's applications."""

        user1 = User.objects.create(
            username='username1', email='username1@example.com', is_active=True)
        user1.set_password('password')
        user1.save()

        application = Application(title='Untitled')
        application.created_by = user1
        application.facility_id = 1
        application.save()

        user2 = User.objects.create(
            username='username2', email='username2@example.com', is_active=True)
        user2.set_password('password')
        user2.save()

        with mock.patch('ldap3.Connection'):
            self.client.login(username='username2@example.com', password='password')

        response = self.client.get(reverse('applications:update', kwargs={
            'pk': application.pk,
        }))
        self.assertTrue(response.status_code==404)

    def test_edit_profile(self):
        user = User.objects.create(
            username='username', email='username@example.com', is_active=True)
        user.set_password('password')
        user.save()

        with mock.patch('ldap3.Connection'):
            self.client.login(username='username@example.com', password='password')

        response = self.client.post(reverse('profile'), {
            'first_name': 'first',
            'last_name': 'last',
            'street': 'street',
            'street2': 'street2',
            'city': 'city',
            'postal_code': 'postal code',
            'country': 'ZA',
            'phone': '1234',
            'institution': 'institution',
            'timezone': 'Africa/Johannesburg',
            'dietary_requirements': 'none',
        }, follow=True)
        self.assertRedirects(response, reverse('dashboard'))
        self.assertContains(response, 'Your profile was updated successfully.')
