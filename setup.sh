#!/bin/bash

pip install -r dev-requirements.txt
cp portal/settings.py.sample portal/settings.py
mkdir media/applications
python manage.py migrate
python manage.py loaddata portal/fixtures/initial_data.json
python manage.py createsuperuser
