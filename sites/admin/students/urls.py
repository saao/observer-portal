import views

from django.conf.urls import include, url


urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^(?P<pk>\d+)$', views.view, name='view'),
    url(r'^(?P<pk>\d+)/cancel$', views.cancel, name='cancel'),
]
