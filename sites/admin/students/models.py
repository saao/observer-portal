from django.conf import settings
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from sites.admin.models import AdminForm


@python_2_unicode_compatible
class TaskCategory(models.Model):
    class Meta:
        app_label = 'portal'
        db_table = 'student_task_category'
        ordering = ['name']

    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class TaskForm(AdminForm):
    class Meta:
        proxy = True
        app_label = 'portal'


class Task(TaskForm):
    class Meta:
        app_label = 'portal'
        db_table = 'student_task'
        ordering = ['-date']
        permissions = (
            ('accept_task', 'Can accept unassigned tasks'),
        )

    title = models.CharField(max_length=255)
    description = models.TextField(null=True, blank=True)
    category = models.ForeignKey(TaskCategory)
    date = models.DateField()
    hours = models.FloatField()

    # A student volunteers to do a task after it has been added.
    student = models.ForeignKey(settings.AUTH_USER_MODEL,
        null=True, blank=True, default=None)
