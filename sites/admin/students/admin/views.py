import csv
import datetime

from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.core.mail import EmailMessage
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission
from django.db.models import Q, Sum
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
from sites.admin.models import AuditLog
from sites.admin.students.models import Task
from sites.admin.students.forms import TaskForm


@login_required
def index(request):
    if not request.user.has_perm('portal.add_task'):
        raise PermissionDenied

    try:
        start_date = datetime.datetime.strptime(request.GET.get('start'), '%Y-%m-%d')
        end_date = datetime.datetime.strptime(request.GET.get('end'), '%Y-%m-%d')
    except (TypeError, ValueError):
        now = datetime.datetime.utcnow()
        start_date, end_date = now - datetime.timedelta(days=30), now

    tasks = Task.objects.filter(
        date__gte=start_date.replace(hour=0, minute=0, second=0),
        date__lte=end_date.replace(hour=23, minute=59, second=59,))

    if 'state' in request.GET:
        if request.GET['state'] == 'available':
            tasks = tasks.filter(state='available')
        elif request.GET['state'] == 'accepted':
            tasks = tasks.filter(state='accepted')
        elif request.GET['state'] == 'completed':
            tasks = tasks.filter(state='completed')
        elif request.GET['state'] == 'confirmed':
            tasks = tasks.filter(state='confirmed')

    return render(request, 'admin/students/index.html', {
        'tasks': tasks,
        'start_date': start_date,
        'end_date': end_date,
    })


@login_required
def create(request):
    if not request.user.has_perm('portal.add_task'):
        raise PermissionDenied

    if request.method == 'POST':
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save(commit=False)
            task.created_by = request.user
            task.state = 'available'
            task.save()

            # Send a notification to all students (users that have the
            # 'accept_task' permission).
            User = get_user_model()

            permission = Permission.objects.get(codename='accept_task')
            users = User.objects.filter(
                Q(groups__permissions=permission) |
                Q(user_permissions=permission)).distinct()

            if users:
                message = render_to_string('students/emails/submitted.txt', {
                    'task': task
                }, request=request)
                to = list(email for email in users.values_list('email', flat=True))
                email = EmailMessage(
                    subject='[{}] {}: {}'.format(task, task.category, task.title),
                    body=message,
                    from_email=settings.DEFAULT_FROM_EMAIL,
                    to=to,
                    reply_to=[request.user.email],
                )
                email.send()

            messages.success(request, 'Your task was added successfully.')

            return redirect('admin:students:index')
    else:
        form = TaskForm()

    return render(request, 'admin/students/create.html', {
        'form': form,
    })


@login_required
def view(request, pk):
    if not request.user.has_perm('portal.add_task'):
        raise PermissionDenied

    task = get_object_or_404(Task, pk=pk)

    if request.POST and 'confirm' in request.POST:
        # Only the person who created the task can confirm it.
        if task.created_by != request.user:
            raise PermissionDenied

        form = TaskForm(request.POST, instance=task)
        if form.is_valid():
            task = form.save(commit=False)
            task.state = 'confirmed'
            task.save()

            AuditLog(
                form=task,
                created_by=request.user,
                from_state='completed',
                to_state='confirmed',
            ).save()

            messages.success(request,
                'Thank you for confirming that the task has been completed.')

            return redirect('admin:students:view', pk=task.pk)
    else:
        form = TaskForm(instance=task)

    return render(request, 'admin/students/view.html', {
        'form': form,
        'task': task,
    })


@login_required
def cancel(request, pk):
    if not request.user.has_perm('portal.add_task'):
        raise PermissionDenied

    task = get_object_or_404(Task,
        Q(created_by=request.user) & (
            ~Q(state='completed') &
            ~Q(state='confirmed') &
            ~Q(state='cancelled')
        ), pk=pk)

    if request.POST:
        AuditLog(
            form=task,
            created_by=request.user,
            from_state='accepted',
            to_state='cancelled',
            reason=request.POST['reason'],
        ).save()

        if 'available' in request.POST:
            task.state = 'available'
            task.student = None

            User = get_user_model()

            permission = Permission.objects.get(codename='accept_task')
            users = User.objects.filter(
                Q(groups__permissions=permission) |
                Q(user_permissions=permission)).distinct()

            if users:
                message = render_to_string('students/emails/submitted.txt', {
                    'task': task
                }, request=request)
                to = list(email for email in users.values_list('email', flat=True))
                email = EmailMessage(
                    subject='[{}] {}: {}'.format(task, task.category, task.title),
                    body=message,
                    from_email=settings.DEFAULT_FROM_EMAIL,
                    to=to,
                    reply_to=[request.user.email],
                )
                email.send()
        else:
            task.state = 'cancelled'

            # Notify student that the task has been cancelled.
            if task.student:
                message = render_to_string('students/emails/cancelled_notification.txt', {
                    'person': request.user,
                    'task': task,
                    'reason': request.POST['reason'],
                }, request=request)
                email = EmailMessage(
                    subject='[{}] {}: {}'.format(task, task.category, task.title),
                    body=message,
                    from_email=settings.DEFAULT_FROM_EMAIL,
                    to=[task.student.email],
                    reply_to=[request.user.email],
                )
                email.send()

        task.save()

        messages.success(request, 'The task has been cancelled.')

        return redirect('admin:students:view', pk=task.pk)

    return render(request, 'admin/students/cancel.html', {
        'task': task,
    })


@login_required
def export(request):
    if not request.user.has_perm('portal.add_task'):
        raise PermissionDenied

    try:
        start_date = datetime.datetime.strptime(request.GET.get('start'), '%Y-%m-%d')
        end_date = datetime.datetime.strptime(request.GET.get('end'), '%Y-%m-%d')
    except (TypeError, ValueError):
        now = datetime.datetime.utcnow()
        start_date, end_date = now - datetime.timedelta(days=30), now

    tasks = Task.objects.filter(
        date__gte=start_date.replace(hour=0, minute=0, second=0),
        date__lte=end_date.replace(hour=23, minute=59, second=59,))

    if 'state' in request.GET:
        if request.GET['state'] == 'available':
            tasks = tasks.filter(state='available')
        elif request.GET['state'] == 'accepted':
            tasks = tasks.filter(state='accepted')
        elif request.GET['state'] == 'completed':
            tasks = tasks.filter(state='completed')
        elif request.GET['state'] == 'confirmed':
            tasks = tasks.filter(state='confirmed')

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="report.csv"'

    writer = csv.writer(response)

    if request.GET.get('report') == 'hours':
        # Create a summary report containing total hours by student.
        tasks = tasks.values(
            'student',
            'student__email',
            'student__first_name',
            'student__last_name').annotate(
                total_hours=Sum('hours')).order_by('student__last_name')

        writer.writerow(['Student', 'Hours'])

        for task in tasks:
            # Tasks that have not been claimed do not have a student associated
            # with them.
            if task['student__email'] is None:
                writer.writerow(['-', task['total_hours'],])
            else:
                writer.writerow([
                    '{} {}'.format(task['student__first_name'].encode('utf-8'), task['student__last_name'].encode('utf-8')),
                    task['total_hours'],
                ])
    else:
        # Create a detailed report containing all tasks.
        writer.writerow(['Task', 'Student', 'Date', 'Hours'])

        for task in tasks:
            writer.writerow([
                task.title,
                task.student,
                task.date,
                task.hours,
            ])

    return response
