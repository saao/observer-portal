import datetime

from icalendar import Calendar, Event
from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.core.mail import EmailMessage
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import render_to_string
from django.db.models import Q
from sites.admin.models import AuditLog
from sites.admin.students.models import Task


@login_required
def index(request):
    if not request.user.has_perm('portal.accept_task'):
        raise PermissionDenied

    # Get all unassigned, available tasks and tasks that this user has accepted.
    tasks = Task.objects.filter((
        Q(date__gte=datetime.date.today()) &
        Q(state='available') &
        Q(student=None)
    ) | Q(student=request.user))

    return render(request, 'students/index.html', {'tasks': tasks})


@login_required
def view(request, pk):
    if not request.user.has_perm('portal.accept_task'):
        raise PermissionDenied

    # The task must be available and unassigned or a task that this user has accepted.
    task = get_object_or_404(Task,
        (Q(state='available') & Q(student=None)) | Q(student=request.user), pk=pk)

    if request.POST:
        if 'accept' in request.POST:
            task.student = request.user
            task.state = 'accepted'
            task.save()

            AuditLog(
                form=task,
                created_by=request.user,
                from_state='available',
                to_state='accepted',
            ).save()

            messages.success(request,
                'Thank you for accepting the task. Please mark it as complete '
                    'when you are done.')

            # Notify person who added the task plus the student's supervisor.
            message = render_to_string('students/emails/accepted_notification.txt', {
                'task': task
            }, request=request)
            email = EmailMessage(
                subject='[{}] {}: {}'.format(task, task.category, task.title),
                body=message,
                from_email=settings.DEFAULT_FROM_EMAIL,
                to=[task.created_by.email],
                cc=([request.POST['supervisor_email']]
                    if request.POST.get('supervisor_email') else []),
                reply_to=[request.user.email],
            )
            email.send()

            # Send a confirmation to the student.
            message = render_to_string('students/emails/accepted_confirmation.txt', {
                'task': task
            }, request=request)
            email = EmailMessage(
                subject='[{}] {}: {}'.format(task, task.category, task.title),
                body=message,
                from_email=settings.DEFAULT_FROM_EMAIL,
                to=[request.user.email],
            )

            calendar = Calendar()
            event = Event()
            event.add('dtstart', task.date)
            event.add('summary', task.title)
            calendar.add_component(event)
            email.attach(
                filename='task.ics',
                content=calendar.to_ical(),
                mimetype='text/calendar',
            )
            email.send()
        elif 'complete' in request.POST:
            task.state = 'completed'
            task.save()

            AuditLog(
                form=task,
                created_by=request.user,
                from_state='accepted',
                to_state='completed',
            ).save()

            messages.success(request, 'Thank you for completing the task.')

            # Notify person who added the task.
            message = render_to_string('students/emails/completed.txt', {
                'task': task
            }, request=request)
            email = EmailMessage(
                subject='[{}] {}: {}'.format(task, task.category, task.title),
                body=message,
                from_email=settings.DEFAULT_FROM_EMAIL,
                to=[task.created_by.email],
                reply_to=[request.user.email],
            )
            email.send()

        return redirect('students:view', pk=task.pk)

    return render(request, 'students/view.html', {'task': task})


@login_required
def cancel(request, pk):

    # The task must be a task that this user has accepted.
    task = get_object_or_404(Task,
        Q(state='accepted') & Q(student=request.user), pk=pk)

    if request.POST:
        task.state = 'available'
        task.student = None
        task.save()

        AuditLog(
            form=task,
            created_by=request.user,
            from_state='accepted',
            to_state='cancelled',
            reason=request.POST['reason'],
        ).save()

        # Notify students that the task is available again.
        if datetime.date.today() <= task.date:
            User = get_user_model()

            permission = Permission.objects.get(codename='accept_task')
            users = User.objects.filter(
                Q(groups__permissions=permission) |
                Q(user_permissions=permission)).distinct()

            if users:
                message = render_to_string('students/emails/submitted.txt', {
                    'task': task
                }, request=request)
                to = list(email for email in users.values_list('email', flat=True))
                email = EmailMessage(
                    subject='[{}] {}: {}'.format(task, task.category, task.title),
                    body=message,
                    from_email=settings.DEFAULT_FROM_EMAIL,
                    to=to,
                    reply_to=[request.user.email],
                )
                email.send()

        # Notify person who added the task.
        message = render_to_string('students/emails/cancelled_notification.txt', {
            'person': request.user,
            'task': task,
            'reason': request.POST['reason'],
        }, request=request)
        email = EmailMessage(
            subject='[{}] {}: {}'.format(task, task.category, task.title),
            body=message,
            from_email=settings.DEFAULT_FROM_EMAIL,
            to=[task.created_by.email],
            reply_to=[request.user.email],
        )
        email.send()

        messages.success(request, 'The task has been cancelled.')

        return redirect('students:view', pk=task.pk)

    return render(request, 'students/cancel.html', {'task': task})
