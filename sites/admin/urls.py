from django.contrib.auth import views
from django.conf import settings
from django.conf.urls import include, url
from portal.views import profile
from portal.admin.views import users, departments, groups
from portal.auth.forms import LoginForm
from sites.admin.views import dashboard


urlpatterns = [
    url(r'^$', dashboard, name='dashboard'),
    url(r'^profile/$', profile, name='profile'),

    url(r'^students/', include('sites.admin.students.urls', namespace='students')),

    url(r'^admin/', include([
        url(r'^students/', include('sites.admin.students.admin.urls', namespace='students')),

        url(r'^users/', include([
            url(r'^$', users.index, name='index'),
            url(r'^new$', users.add, name='add'),
            url(r'^(?P<pk>\d+)/$', users.edit, name='edit'),
        ], namespace='users')),

        url(r'^departments/', include([
            url(r'^$', departments.index, name='index'),
            url(r'^new$', departments.create, name='create'),
            url(r'^(?P<pk>\d+)/$', departments.update, name='update'),
        ], namespace='departments')),

        url(r'^groups/', include([
            url(r'^$', groups.index, name='index'),
            url(r'^new$', groups.add, name='add'),
            url(r'^(?P<pk>\d+)/$', groups.edit, name='edit'),
        ], namespace='groups')),
    ], namespace='admin')),

    url(r'^login/$', views.login,
        {'template_name': 'auth/login.html', 'authentication_form': LoginForm}, name='login'),
    url(r'^logout/$', views.logout, {'next_page': '/'}, name='logout'),
]


if settings.DEBUG:
    from django.views import static
    from django.views.generic.base import TemplateView
    urlpatterns += [
        url(r'^403$', TemplateView.as_view(template_name='403.html')),
        url(r'^404$', TemplateView.as_view(template_name='404.html')),
        url(r'^500$', TemplateView.as_view(template_name='500.html')),
        url(r'^docs/(?P<path>.*)$', static.serve, {
            'document_root': 'docs/_build/html/',
        }),
        url(r'^media/(?P<path>.*)$', static.serve, {
            'document_root': settings.MEDIA_ROOT,
        }),
        url(r'^static/(?P<path>.*)$', static.serve, {
            'document_root': settings.STATIC_ROOT,
        }),
   ]
