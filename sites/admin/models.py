import io
import os
import json

from datetime import datetime
from decimal import Decimal
from django.conf import settings
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from polymorphic.models import PolymorphicModel


class CustomJSONEncoder(json.JSONEncoder):
    """Handles serialisation of datetime and Decimal objects."""

    def default(self, o):
        if isinstance(o, datetime):
            return o.strftime('%Y-%m-%d %H:%M')
        elif isinstance(o, Decimal):
            return '{0:.2f}'.format(o)

        return str(o)


@python_2_unicode_compatible
class AdminForm(PolymorphicModel):
    """A generic model to store data for different types of forms."""

    class Meta:
        db_table = 'admin_form'
        app_label = 'portal'
        ordering = ['-created_at']

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL)
    state = models.CharField(max_length=255, default='draft')

    # We'll wrap access to this using a property but it's important to remember
    # that we need to set 'data' directly (eg. instance.data = {'foo': 'bar'})
    # instead of setting individual keys (eg. instance.data['foo'] = 'bar')
    # because it bypasses 'set_data'.
    _data = models.TextField(db_column='data', null=True, blank=True)

    def __str__(self):
        if self.pk:
            return '{:06}'.format(self.pk)
        return super(AdminForm, self).__str__()

    def get_data(self):
        if not hasattr(self, '_cached_data'):
            self._cached_data = json.loads(self._data or '{}')
        return self._cached_data

    def set_data(self, data):
        if hasattr(self, '_cached_data'):
            del self._cached_data
        self._data = json.dumps(data, cls=CustomJSONEncoder)

    data = property(get_data, set_data)


class AuditLog(models.Model):
    """Records form state transitions over time."""

    class Meta:
        db_table = 'admin_audit_log'
        app_label = 'portal'
        ordering = ['created_at']

    form = models.ForeignKey(AdminForm, related_name='audit_log')
    created_at = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL)
    from_state = models.CharField(max_length=255)
    to_state = models.CharField(max_length=255)

    # A user can optionally supply a reason for the transition eg. explaining
    # why a form was rejected.
    reason = models.TextField(null=True, blank=True)


class Training(AdminForm):
    class Meta:
        proxy = True
        app_label = 'portal'

    def as_pdf(self):
        path = os.path.join(
            settings.MEDIA_ROOT, 'forms', 'training-{}.pdf'.format(
                self)).encode('utf-8')

        if os.path.exists(path):
            with open(path) as f:
                buffer = io.BytesIO(f.read())
        else:
            # TODO:
            buffer = io.BytesIO()

        return buffer.getvalue()
