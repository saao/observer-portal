from django import forms


class RequiredFormSet(forms.BaseInlineFormSet):
    """Makes sure that at least one entry is supplied in a formset."""

    def __init__(self, *args, **kwargs):
        super(RequiredFormSet, self).__init__(*args, **kwargs)
        try:
            self.forms[0].empty_permitted = False
        except IndexError:
            pass

    def clean(self):
        if any(self.errors):
            return

        for form in self.forms:
            if not form.cleaned_data or form.cleaned_data['DELETE']:
                continue

