# OBSERVER PORTAL

The observer portal is a general purpose web application and a central point of
entry meant to be used at the SAAO to manage its observing activities.

## Contributing

When contributing any Python code, please take care to follow the [Python style guide](https://www.python.org/dev/peps/pep-0008/).

### Report a problem or make a suggestion

Go to our [issue tracker](https://bitbucket.org/saao/observer-portal/issues)
and check if your problem/suggestion is already reported. If not, create a new
issue with a descriptive title and detail your suggestion or steps to reproduce
the problem.

### Contribute to the code

If you know how to code, we welcome you to send fixes and new features, but in
order to be efficient we ask you to follow the following procedure:

* Fork this repo on Bitbucket using the "Fork" button.
* Clone your forked repo locally.

``$ git clone git@bitbucket.org:yourname/observer-portal.git portal``

* Don't modify or work on the master branch, we'll use it to always be in sync
  with observer-portal upstream.

```
$ git remote add upstream git@bitbucket.org:saao/observer-portal.git
$ git fetch upstream
```

* Always create a new issue when you plan to work on a bug or new feature and
  wait for other devs input before you start coding.
* Once the new feature is approved or the problem confirmed, go to your local
  copy and create a new branch to work on it. Use a descriptive name for it,
  include the issue number for reference.

``$ git checkout -b improve-contacts-99``

* Do your coding and push it to your fork. Include as few commits as possible
  (one should be enough) and a good description. Always include a reference to
  the issue with "Fixes #number".

```
$ git add .
$ git commit -m "Improved contact list. Fixes #99"
$ git push origin improve-contacts-99
```

* Do a new pull request from your "improve-contacts-99" branch to
  observer-portal "master".

#### How to make changes suggested on a pull request

Sometimes when you do a PR, you will be asked to correct some code. You can do
it on your work branch and commit normally, PR will be automatically updated.

``$ git commit --amend "Oops, fixing typo"``

Once everything is OK, you will be asked to merge all commit messages into one
to keep history clean.

``$ git rebase --interactive master``

Edit the file and mark as fixup (f) all commits you want to merge with the
first one:

```
pick 1c85e07 Improved contact list. Fix #99
f c595f79 Oops, fixing typo
```

Once rebased you can force a push to your fork branch and the PR will be
automatically updated.

``$ git push origin improve-contacts-99 --force``

#### How to keep your local branches updated

To keep your local master branch updated with upstream master, regularly do:

```
$ git fetch upstream
$ git checkout master
$ git pull --rebase upstream master
```

To update the branch you are coding in:

```
$ git checkout improve-contacts-99
$ git rebase master
```
